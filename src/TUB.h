/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libtube
* FILE NAME: TUB.h
* BRANCH NAME: main
* 
* CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
* Possibility to parameterize the number of tubes locally.
*
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2023 Contributors to the libtube library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/
#include "param_tube.h"
#include "struct_tube.h"
#include "functions_tube.h"
