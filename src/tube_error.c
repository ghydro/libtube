/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libtube
* FILE NAME: tube_error.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
* Possibility to parameterize the number of tubes locally.
*
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2023 Contributors to the libtube library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"

#include "HYD.h"
#include "TUB.h"

int error_geocoord_TUB(s_georef_tub *geocoords) // Test if some tube may be smashed..
{
  int i, j;

  for (i=0; i<N_VERTEX; i++)
    for (j=0; j<N_VERTEX; j++)
      if (i != j && fabs(geocoords->vertices[i][0] - geocoords->vertices[j][0]) < EPS1_TUB && fabs(geocoords->vertices[i][1] - geocoords->vertices[j][1]) < EPS1_TUB)
	  return 1;
  
  return 0;
}
