/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libtube
* FILE NAME: functions_tube.h
* BRANCH NAME: main
* 
* CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
* Possibility to parameterize the number of tubes locally.
*
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2023 Contributors to the libtube library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/
#ifndef _FUNCTIONS_TUBE_H_
#define _FUNCTIONS_TUBE_H_

// tube_build.c
double *interpolation_beyond_left_bank(double, s_cross_section_tub *, double);
double *interpolation_beyond_right_bank(double, s_cross_section_tub *, double);
double *wet_param_from_left_bank_TUB(double, double, s_cross_section_tub *, double);
double *wet_param_from_right_bank_TUB(double, double, s_cross_section_tub *, double);
double energy_grad_line_slope_TUB(double, double, double);
double left_integrated_velocity_TUB(double*, double, double);
double right_integrated_velocity_TUB(double*, double, double);
double cumulated_flow_rate_from_left_bank_TUB(double, double);
double cumulated_flow_rate_from_right_bank_TUB(double, double);
double profile_filling(double, s_face_hyd *, s_cross_section_tub *, double); // may create conflicts with libhyd as the profile extremities are not defined the same way
double profile_overflow(double, s_cross_section_tub *, double); // define new profile points at extremities if the water level overflows the banks and returns the eventual width of the stream
double equi_flow_rate_distance_TUB(s_cross_section_tub *, double, double, double, double, double, double);
double mid_flow_back_up_algorithm(double, double, s_cross_section_tub *, double, double, double);

// tube_properties.c
s_cross_section_tub *profile_building_TUB(s_face_hyd *, int);
double back_up_algorithm(double, double, double, double, double, double, double, s_cross_section_tub *, double, double, double); // in case Broyden's algo does not converge
s_cross_section_tub *backup_profile_building_TUB(s_face_hyd **, int);
s_tube_param_tub **tubes_param_over_one_face_TUB(s_cross_section_tub *);
void update_param_over_one_face_TUB(s_def_tub **, s_face_hyd **, int);
void check_geom_chains(s_face_hyd *);

// tube_domain.c
s_def_tub ***build_tube_domain_per_reach_TUB(s_reach_hyd *, int *);
void update_tube_domain_TUB(s_def_tub ****, s_chyd *);
s_def_tub ****build_tube_domain_for_all_reaches_TUB(s_chyd *, int);
void build_singularities_linkage_TUB(s_chyd *, s_def_tub ****);
void reduce_tube_number(s_chyd *);

// tube_georef.c
void up_georeferencing_TUB(s_def_tub **, int, s_cross_section_tub *);
void down_georeferencing_TUB(s_def_tub **, int, s_cross_section_tub *);
void centre_georeferencing_TUB(s_def_tub *);

// tube_ouputs.c
void write_wtk_file_for_sig_TUB(s_inout_set_io *, double, s_chyd *, s_chronos_CHR *, s_def_tub ****, FILE *);
void time_geo_outputs_TUB(s_inout_set_io *, double, s_chyd *, s_def_tub ****, FILE *);
s_output_tube_type *TUB_chain_output_bck_tube(s_output_tube_type *, s_output_tube_type *);

// tube_error.c
int error_geocoord_TUB(s_georef_tub *);

// tube_dealloc.c
void free_param_TUB(s_tube_param_tub *);
void free_reach_connection_TUB(s_reach_connect_tub *);
void free_georeferences_TUB(s_georef_tub *);
void free_profile_TUB(s_cross_section_tub *);
void deallocate_one_TUB(s_def_tub *);

#endif
