#-------------------------------------------------------------------------------
# 
# LIBRARY NAME: libtube
# FILE NAME: update_version.sh
# BRANCH NAME: main
# 
# CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
# 
# PROJECT MANAGER: Nicolas FLIPO
#
# LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
# Possibility to parameterize the number of tubes locally.
#
# ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
#
# COPYRIGHT: (c) 2023 Contributors to the libtube library. 
# CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
#          
# 
# All rights reserved. This Library and the accompanying materials
# are made available under the terms of the Eclipse Public License v2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v20.html
# 
#------------------------------------------------------------------------------

#Automatic change of the version number
#> update_version.sh <new_version_number>

#gawk -f update_makefile.awk -v nversion=$1 Makefile
#mv awk.out Makefile

gawk -f update_param.awk -v nversion=$1 param_tube.h
mv awk.out param_tube.h

gawk -f update_doxygen.awk -v nversion=$1 test_doxygen
mv awk.out test_doxygen

gawk -f update_makefile.awk -v nversion=$1 Makefile
mv awk.out Makefile

echo "Version number updated in param.h, doxygen and Makefile cmd file to" $1
