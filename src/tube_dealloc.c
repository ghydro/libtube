/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libtube
* FILE NAME: tube_dealloc.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
* Possibility to parameterize the number of tubes locally.
*
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2023 Contributors to the libtube library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"

#include "HYD.h"
#include "TUB.h"

void free_param_TUB(s_tube_param_tub *param)
{
  free(param->pcoord);
  free(param);
  return;
}

void free_reach_connection_TUB(s_reach_connect_tub *connection)
{
  free(connection->neigh_tubes);
  free(connection->frac_neigh_tubes);
  free(connection);
  return;
}

void free_georeferences_TUB(s_georef_tub *geocoords)
{
  int i;
  
  for (i=0; i<N_VERTEX; i++)
    free(geocoords->vertices[i]);
  free(geocoords->centre);
  free(geocoords);
  return;
}

void free_profile_TUB(s_cross_section_tub *cross_section)
{
  free(cross_section->p_coord_tube);
  free(cross_section);
  return;
}

void deallocate_one_TUB(s_def_tub *tube)
{
  free_param_TUB(tube->param_amont);
  free_param_TUB(tube->param_aval);
  free_reach_connection_TUB(tube->connect_bief);
  free_georeferences_TUB(tube->geocoords);
  free(tube);
  return;
}
