/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libtube
* FILE NAME: tube_georef.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
* Possibility to parameterize the number of tubes locally.
*
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2023 Contributors to the libtube library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"

#include "HYD.h"
#include "TUB.h"

void up_georeferencing_TUB(s_def_tub **tube, int n_tube, s_cross_section_tub *profile)
{
  int t;
  double x0, y0, x1, y1;
  s_georef_tub *ref;
  double width, dist_l, dist_r;
  s_pointXYZ_hyd *q;

  if (profile->type == ABSC_Z)
    {
      for (t=0; t<n_tube; t++)
	tube[t]->geocoords = NULL;
    }
  else if (profile->type == X_Y_Z)
    {
      q = profile->pptsXYZ;
      while (q->prev != NULL)
	q = q->prev;
      x0 = q->x;
      y0 = q->y;
      while (q->next != NULL)
	q = q->next;
      x1 = q->x;
      y1 = q->y;
      /****/
      width = sqrt((x1-x0)*(x1-x0) + (y1-y0)*(y1-y0));

      for (t=0; t<n_tube; t++)
	{
	  ref = (s_georef_tub *)malloc(sizeof(s_georef_tub)); // Warning: allocation done only when calling up_georeferencing!
	  //ref->vertices = (double **)malloc(sizeof(double*)*N_VERTEX);
	  ref->vertices[LEFT_UP] = (double *)malloc(sizeof(double)*2);
	  ref->vertices[RIGHT_UP] = (double *)malloc(sizeof(double)*2);
	  /****/
	  dist_l = tube[t]->param_amont->pcoord[0];
	  dist_r = tube[t]->param_amont->pcoord[1];
	  ref->vertices[LEFT_UP][0] = x0 + dist_l*(x1 - x0)/width; ref->vertices[LEFT_UP][1] = y0 + dist_l*(y1 - y0)/width;
	  ref->vertices[RIGHT_UP][0] = x0 + dist_r*(x1 - x0)/width; ref->vertices[RIGHT_UP][1] = y0 + dist_r*(y1 - y0)/width;
	  /****/
	  tube[t]->geocoords = ref;
	}
    }
  return;
}

void down_georeferencing_TUB(s_def_tub **tube, int n_tube, s_cross_section_tub *profile)
{
  int t;
  double x0, y0, x1, y1;
  double width, dist_l, dist_r;
  s_pointXYZ_hyd *q;
  
  if (profile->type == ABSC_Z)
    {
      for (t=0; t<n_tube; t++)
	tube[t]->geocoords = NULL;
    }
  else if (profile->type == X_Y_Z)
    {
      q = profile->pptsXYZ;
      while (q->prev != NULL)
	q = q->prev;
      x0 = q->x;
      y0 = q->y;
      while (q->next != NULL)
	q = q->next;
      x1 = q->x;
      y1 = q->y;
      /****/
      width = sqrt((x1-x0)*(x1-x0) + (y1-y0)*(y1-y0));
      
      for (t=0; t<n_tube; t++)
	{
	  tube[t]->geocoords->vertices[LEFT_DOWN] = (double *)malloc(sizeof(double)*2);
	  tube[t]->geocoords->vertices[RIGHT_DOWN] = (double *)malloc(sizeof(double)*2);
	  dist_l = tube[t]->param_aval->pcoord[0];
	  dist_r = tube[t]->param_aval->pcoord[1];
	  tube[t]->geocoords->vertices[LEFT_DOWN][0] = x0 + dist_l*(x1 - x0)/width; tube[t]->geocoords->vertices[LEFT_DOWN][1] = y0 + dist_l*(y1 - y0)/width;
	  tube[t]->geocoords->vertices[RIGHT_DOWN][0] = x0 + dist_r*(x1 - x0)/width; tube[t]->geocoords->vertices[RIGHT_DOWN][1] = y0 + dist_r*(y1 - y0)/width;
	  centre_georeferencing_TUB(tube[t]); // And finally, central coordinates !
	}
    }
  return;
}

void centre_georeferencing_TUB(s_def_tub *tube)
{
  int i;
  double x = 0., y = 0.;

  tube->geocoords->centre = (double *)malloc(sizeof(double)*2);
  for (i=0; i<N_VERTEX; i++)
    {
      x = x + tube->geocoords->vertices[i][0];
      y = y + tube->geocoords->vertices[i][1];
    }
  tube->geocoords->centre[0] = x/4.;
  tube->geocoords->centre[1] = y/4.;
}
