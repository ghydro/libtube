/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libtube
* FILE NAME: param_tube.h
* BRANCH NAME: main
* 
* CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
* Possibility to parameterize the number of tubes locally.
*
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2023 Contributors to the libtube library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/
//Version number of the present library
#define VERSION_TUBE 0.08

/** @def EPS's */
#define EPS1_TUB 0.000001
#define EPS2_TUB EPS1_TUB

/** Elder's non-dimensional coeffcients for diffusion **/
#define ALPHA_X 6.
#define ALPHA_Y 0.92

/** Gravity (m/s2) **/
#define GRAVITY 9.81

/** Number of tubes **/
#define NTUBE_DEFAULT 6
#define NTUBE_MIN 3

/** Non-convergence parameter allowing to launch emergency algorithm **/
#define DIV_PARAM 41

/** Directions for neighbouring tubes **/
enum directions_tube {LEFT_TUB,RIGHT_TUB,UPWARD_TUB,DOWNWARD_TUB,N_NEIGH_DIR};

/** Vertices on each tube **/
enum vertices {LEFT_UP,RIGHT_UP,LEFT_DOWN,RIGHT_DOWN,N_VERTEX};
