/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libtube
* FILE NAME: tube_build.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
* Possibility to parameterize the number of tubes locally.
*
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2023 Contributors to the libtube library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"

#include "HYD.h"
#include "TUB.h"

double *interpolation_beyond_left_bank(double w_level, s_cross_section_tub *profile, double height_diff)
{
  double *rslt, k, w_absc;
  s_pointAbscZ_hyd *p0, *p1;
  s_pointXYZ_hyd *q0, *q1;
  double absc0, absc1, z0, z1;
  rslt = (double *)malloc(sizeof(double)*2);

  switch(profile->type)
    {
    case ABSC_Z :
      p0 = profile->pptsAbscZ;
      while (p0->prev != NULL)
	p0 = p0->prev;
      p1 = p0->next;
      absc0 = 0.;
      absc1 = fabs(p1->absc - p0->absc);
      z0 = p0->z - height_diff;
      z1 = p1->z - height_diff;
      break;
    
    case X_Y_Z :      
      q0 = profile->pptsXYZ;
      while (q0->prev != NULL)
	q0 = q0->prev;
      q1 = q0->next;
      absc0 = 0.;
      absc1 = sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y));
      z0 = q0->z - height_diff;
      z1 = q1->z - height_diff;
      break;
    }

  if (z0 > w_level)
    rslt[0] = rslt[1] = 0.;
  else
    {
      if (z0 <= z1)
	printf("Pb with left coord. of river cross-section: there might be infinite interpolation!\n");
      else
	{
	  k = (z0 - w_level)/(z1 - z0);
	  w_absc = absc0 - k*(absc1 - absc0);
	  rslt[0] = (absc0 - w_absc)*(w_level - z0)/2.;
	  rslt[1] = sqrt((absc0 - w_absc)*(absc0 - w_absc) + (w_level - z0)*(w_level - z0));
	}
    }
  return rslt;
}

double *interpolation_beyond_right_bank(double w_level, s_cross_section_tub *profile, double height_diff)
{
  double *rslt, k, w_absc;
  s_pointAbscZ_hyd *p0, *p1;
  s_pointXYZ_hyd *q0, *q1;
  double absc0, absc1, z0, z1;
  rslt = (double *)malloc(sizeof(double)*2);

  switch(profile->type)
    {
    case ABSC_Z :
      p0 = profile->pptsAbscZ;
      while (p0->next != NULL)
	p0 = p0->next;
      p1 = p0->prev;
      absc0 = 0.;
      absc1 = fabs(p1->absc - p0->absc);
      z0 = p0->z - height_diff;
      z1 = p1->z - height_diff;
      break;
    
    case X_Y_Z :      
      q0 = profile->pptsXYZ;
      while (q0->next != NULL)
	q0 = q0->next;
      q1 = q0->prev;
      absc0 = 0.;
      absc1 = sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y));
      z0 = q0->z - height_diff;
      z1 = q1->z - height_diff;
      break;
    }

  if (z0 > w_level)
    rslt[0] = rslt[1] = 0.;
  else
    {
      if (z0 <= z1)
	printf("Pb with right coord. of river cross-section: there might be infinite interpolation!\n");
      else
	{
	  k = (z0 - w_level)/(z1 - z0);
	  w_absc = absc0 - k*(absc1 - absc0);
	  rslt[0] = (absc0 - w_absc)*(w_level - z0)/2.;
	  rslt[1] = sqrt((absc0 - w_absc)*(absc0 - w_absc) + (w_level - z0)*(w_level - z0));
	}
    }
  return rslt;
}

double *wet_param_from_left_bank_TUB(double w_level, double dist_from_bank_l, s_cross_section_tub *profile, double height_diff)
{
  double  A, P_wet, lb_absc, pente, w_level_absc;
  s_pointAbscZ_hyd *p0, *p1;
  s_pointXYZ_hyd *q0, *q1;
  double control_point_absc, control_point_z;
  double absc0, absc1, z0, z1;
  int pente_ind;
  double *wet_param;
  
  wet_param = (double *)malloc(sizeof(double)*2);
  
  A = 0.;
  P_wet = 0.;
  control_point_absc = dist_from_bank_l;
  
  switch(profile->type)
    {
    case ABSC_Z :
      p0 = profile->pptsAbscZ;
      while (p0->prev != NULL)
	p0 = p0->prev;
      p1 = p0->next;
      absc0 = 0.;
      absc1 = fabs(p1->absc - p0->absc);
      z0 = p0->z - height_diff;
      z1 = p1->z - height_diff;
      lb_absc = absc0;
      break;
    
    case X_Y_Z :      
      q0 = profile->pptsXYZ;
      while (q0->prev != NULL)
	q0 = q0->prev;
      q1 = q0->next;
      //absc0 = sqrt(q0->x*q0->x + q0->y*q0->y); WRONG !!!
      absc0 = 0.;
      //absc1 = sqrt(q1->x*q1->x + q1->y*q1->y); WRONG !!!
      absc1 = sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y));
      z0 = q0->z - height_diff;
      z1 = q1->z - height_diff;
      lb_absc = absc0;
      break;
    }
  
  while (fabs(absc0-lb_absc) <= control_point_absc + 0.0001)
    {
      if ((profile->type == ABSC_Z && p1 != NULL) || (profile->type == X_Y_Z && q1 != NULL))
	{
	  
	  //printf("\n%lf %lf %lf %lf\n",absc0,z0,absc1,z1);
	  pente_ind = 0;
	  if (fabs(absc1 - absc0) < 0.000001) // Slope control in case it tends toward infinity
	    pente_ind = 1; 
	  else
	    pente = (z1 - z0)/(absc1 - absc0);
	  
	  if (pente_ind == 1) /* vertical slope */
	    /*** --> do nothing for A: go to next iter ***/
	    {
	      if (z1 < z0)
		{
		  if (w_level > z0)
		    P_wet += z0 - z1;
		  else
		    if (w_level > z1)
		      {
			P_wet += w_level - z1;
			w_level_absc = absc0;
		      }
		}
	      else
		{
		  if (w_level > z1)
		    P_wet += z1 - z0;
		  else
		    if (w_level > z0)
		      P_wet += w_level - z0;
		}
	    }
	  else /* other slope cases */
	    {
	      control_point_z = z0 - pente*(absc0 - control_point_absc);
	      if ((z0 >= w_level && z1 <= w_level) || (z0 <= w_level && z1 >= w_level))
		w_level_absc = (w_level - (z0 - pente*absc0)) / pente;
	      if (z0 > z1) /* 1. control the slope orientation: going up or going down */
		{
		  if (w_level > z0) /* 2. control the water level relatively to the cross-section part considered */ 
		    {
		      if (fabs(absc1-lb_absc) > control_point_absc) /* 3. control whether the end of the cross-section part is overtaken */
			{
			  //printf("A\n");
			  A += (w_level - z0)*(control_point_absc - fabs(absc0-lb_absc));
			  A += (control_point_absc - fabs(absc0-lb_absc))*(z0 - control_point_z)/2.;
			  P_wet += sqrt((control_point_absc - fabs(absc0-lb_absc))*(control_point_absc - fabs(absc0-lb_absc))+(z0 - control_point_z)*(z0 - control_point_z));
			}
		      else /* 3. */
			{
			  //printf("B\n");
			  A += (w_level - z0)*fabs(absc0 - absc1);
			  A += (z0 - z1)*fabs(absc0 - absc1)/2.;
			  P_wet += sqrt((absc0 - absc1)*(absc0 - absc1) + (z0 - z1)*(z0 - z1));
			}
		    }
		  else if (w_level <= z0 && w_level >= z1) /* 2. */
		    {
		      if (fabs(w_level_absc-lb_absc) >= control_point_absc) /* 3. */
			{
			  //printf("C\n");
			  /*** go to next iter ***/
			}
		      else if (fabs(w_level_absc-lb_absc) < control_point_absc && control_point_absc < fabs(absc1-lb_absc)) /* 3. */
			{
			  //printf("D\n");
			  A += (w_level - control_point_z)*fabs(control_point_absc - w_level_absc)/2.;
			  P_wet += sqrt((w_level - control_point_z)*(w_level - control_point_z) + (control_point_absc - w_level_absc)*(control_point_absc - w_level_absc));
			}
		      else /* 3. */
			{
			  //printf("E\n");
			  A += (w_level - z1)*fabs(absc1 - w_level_absc)/2.;
			  P_wet += sqrt((w_level - z1)*(w_level - z1) + (absc1 - w_level_absc)*(absc1 - w_level_absc));
			}
		    }
		  else if (z1 > w_level) /* 2. */
		    {
		      //printf("F\n");
		      /*** go to next iter ***/
		    }
		}
	      else /* 1. (z0 < z1) */
		{
		  if (w_level > z1) /* 2. */
		    {
		      if (fabs(absc1-lb_absc) > control_point_absc) /* 3. */
			{
			  //printf("A2\n");
			  A += (w_level - control_point_z)*(control_point_absc - fabs(absc0-lb_absc));
			  A += (control_point_absc - fabs(absc0-lb_absc))*(control_point_z - z0)/2.;
			  P_wet += sqrt((control_point_absc - fabs(absc0-lb_absc))*(control_point_absc - fabs(absc0-lb_absc)) + (z0 - control_point_z)*(z0 - control_point_z));
			}
		      else /* 3. */
			{
			  //printf("B2\n");
			  A += (w_level - z1)*fabs(absc1 - absc0);
			  A += (z1 - z0)*fabs(absc1 - absc0)/2.;
			  P_wet += sqrt((absc1 - absc0)*(absc1 - absc0) + (z0 - z1)*(z0 - z1));
			}
		    }
		  else if (w_level <= z1 && w_level >= z0) /* 2. */
		    {
		      if (fabs(w_level_absc-lb_absc) > control_point_absc) /* 3. */
			{
			  //printf("C2\n");
			  A += (w_level - control_point_z)*(control_point_absc - fabs(absc0-lb_absc));
			  A += (control_point_absc - fabs(absc0-lb_absc))*(control_point_z - z0)/2.;
			  P_wet += sqrt((control_point_absc - fabs(absc0-lb_absc))*(control_point_absc - fabs(absc0-lb_absc)) + (z0 - control_point_z)*(z0 - control_point_z));
			}
		      else
			{
			  //printf("D2\n");
			  A += (w_level - z0)*fabs(absc0 - w_level_absc)/2.;
			  P_wet += sqrt((w_level - z0)*(w_level - z0) + (absc0 - w_level_absc)*(absc0 - w_level_absc));
			}
		    }
		  else
		    {
		      //printf("E2\n");
		      /*** go to next iter ***/ 
		    }
		}
	    }
	  //printf("%lf %lf\n",A,P_wet);
	  
	  switch(profile->type) {
	  case ABSC_Z :
	    p0 = p1;
	    p1 = p1->next;
	    if (p1 != NULL)
	      {
		absc0 = absc1;
		absc1 = absc0 + fabs(p1->absc - p0->absc);
		z0 = p0->z - height_diff;
		z1 = p1->z - height_diff;
	      }
	    break;
	  case X_Y_Z :
	    q0 = q1;
	    q1 = q1->next;
	    if (q1 != NULL)
	      {
		absc0 = absc1;
		absc1 = absc1 + sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y));
		z0 = q0->z - height_diff;
		z1 = q1->z - height_diff;
	      }
	    break;	
	  }      
	}
      else
	break;
    }
  
  /*rslt = interpolation_beyond_left_bank(w_level, dist_from_bank_l, profile, height_diff);
  A = A + rslt[0];
  P_wet = P_wet + rslt[1];
  free(rslt);*/
  wet_param[0] = A;
  wet_param[1] = P_wet;
  return wet_param;
}

double *wet_param_from_right_bank_TUB(double w_level, double dist_from_bank_l, s_cross_section_tub *profile, double height_diff)
{
  double  A, P_wet, rb_absc, pente, w_level_absc;
  s_pointAbscZ_hyd *p0, *p1;
  s_pointXYZ_hyd *q0, *q1;
  double control_point_absc, control_point_z, tot_width;
  double absc0, absc1, z0, z1;
  int pente_ind;
  double *wet_param;
  
  wet_param = (double *)malloc(sizeof(double)*2);
  
  A = 0.;
  P_wet = 0.;
  
  switch(profile->type)
    {
    case ABSC_Z :
      p0 = profile->pptsAbscZ;
      while (p0->prev != NULL)
	p0 = p0->prev;
      absc0 = p0->absc;
      while (p0->next != NULL)
	p0 = p0->next;
      absc1 = p0->absc;
      tot_width = fabs(absc1-absc0);
      
      p1 = p0->prev;
      absc0 = 0;
      absc1 = fabs(p1->absc - p0->absc);
      z0 = p0->z - height_diff;
      z1 = p1->z - height_diff;
      rb_absc = 0.;
      break;
    
    case X_Y_Z :      
      q0 = profile->pptsXYZ;
      while (q0->prev != NULL)
	q0 = q0->prev;
      q1 = q0;
      while (q0->next != NULL)
	q0 = q0->next;
      tot_width = sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y));
      
      q1 = q0->prev;
      absc0 = 0;
      absc1 = sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y));
      z0 = q0->z - height_diff;
      z1 = q1->z - height_diff;
      rb_absc = 0.;
      break;
    }

  control_point_absc = tot_width - dist_from_bank_l;
  
  while (absc0 <= control_point_absc + 0.0001)
    {
      if ((profile->type == ABSC_Z && p1 != NULL) || (profile->type == X_Y_Z && q1 != NULL))
	{
	  //printf("\n%lf %lf %lf %lf\n",absc0,z0,absc1,z1);
	  pente_ind = 0;
	  if (absc1 - absc0 < 0.000001) // Slope control in case it tends toward infinity
	    pente_ind = 1; 
	  else
	    pente = (z1 - z0)/(absc1 - absc0);
	  
	  if (pente_ind == 1) /* vertical slope */
	    /*** --> do nothing for A: go to next iter ***/
	    {
	      if (z1 < z0)
		{
		  if (w_level > z0)
		    P_wet += z0 - z1;
		  else
		    if (w_level > z1)
		      {
			P_wet += w_level - z1;
			w_level_absc = absc0;
		      }
		}
	      else
		{
		  if (w_level > z1)
		    P_wet += z1 - z0;
		  else
		    if (w_level > z0)
		      P_wet += w_level - z0;
		}
	    }
	  else /* other slope cases */
	    {
	      control_point_z = z0 + pente*(control_point_absc - absc0);
	      if ((z0 >= w_level && z1 <= w_level) || (z0 <= w_level && z1 >= w_level))
		w_level_absc = (w_level - (z0 - pente*absc0)) / pente;	
	      if (z0 > z1) /* 1. control the slope orientation: going up or going down */
		{
		  if (w_level > z0) /* 2. control the water level relatively to the cross-section part considered */ 
		    {
		      if (fabs(absc1-rb_absc) > control_point_absc) /* 3. control whether the end of the cross-section part is overtaken */
			{
			  //printf("A\n");
			  A += (w_level - z0)*(control_point_absc - fabs(absc0-rb_absc));
			  A += (control_point_absc - fabs(absc0-rb_absc))*(z0 - control_point_z)/2.;
			  P_wet += sqrt((control_point_absc - fabs(absc0-rb_absc))*(control_point_absc - fabs(absc0-rb_absc))+(z0 - control_point_z)*(z0 - control_point_z));
			}
		      else /* 3. */
			{
			  //printf("B\n");
			  A += (w_level - z0)*fabs(absc0 - absc1);
			  A += (z0 - z1)*fabs(absc0 - absc1)/2.;
			  P_wet += sqrt((absc0 - absc1)*(absc0 - absc1) + (z0 - z1)*(z0 - z1));
			}
		    }
		  else if (w_level <= z0 && w_level >= z1) /* 2. */
		    {
		      if (fabs(w_level_absc-rb_absc) >= control_point_absc) /* 3. */
			{
			  //printf("C\n");
			  /*** go to next iter ***/
			}
		      else if (fabs(w_level_absc-rb_absc) < control_point_absc && control_point_absc < fabs(absc1-rb_absc)) /* 3. */
			{
			  //printf("D w_level %lf c_p_z %lf c_p_absc %lf w_l_absc %lf\n",w_level,control_point_z,control_point_absc,w_level_absc);
			  A += (w_level - control_point_z)*fabs(control_point_absc - w_level_absc)/2.;
			  P_wet += sqrt((w_level - control_point_z)*(w_level - control_point_z) + (control_point_absc - w_level_absc)*(control_point_absc - w_level_absc));
			}
		      else /* 3. */
			{
			  //printf("E\n");
			  A += (w_level - z1)*fabs(absc1 - w_level_absc)/2.;
			  P_wet += sqrt((w_level - z1)*(w_level - z1) + (absc1 - w_level_absc)*(absc1 - w_level_absc));
			}
		    }
		  else if (z1 > w_level) /* 2. */
		    {
		      //printf("F\n");
		      /*** go to next iter ***/
		    }
		}
	      else /* 1. (z0 < z1) */
		{
		  if (w_level > z1) /* 2. */
		    {
		      if (fabs(absc1-rb_absc) > control_point_absc) /* 3. */
			{
			  //printf("A2\n");
			  A += (w_level - control_point_z)*(control_point_absc - fabs(absc0-rb_absc));
			  A += (control_point_absc - fabs(absc0-rb_absc))*(control_point_z - z0)/2.;
			  P_wet += sqrt((control_point_absc - fabs(absc0-rb_absc))*(control_point_absc - fabs(absc0-rb_absc)) + (z0 - control_point_z)*(z0 - control_point_z));
			}
		      else /* 3. */
			{
			  //printf("B2\n");
			  A += (w_level - z1)*fabs(absc1 - absc0);
			  A += (z1 - z0)*fabs(absc1 - absc0)/2.;
			  P_wet += sqrt((absc1 - absc0)*(absc1 - absc0) + (z0 - z1)*(z0 - z1));
			}
		    }
		  else if (w_level <= z1 && w_level >= z0) /* 2. */
		    {
		      if (fabs(w_level_absc-rb_absc) > control_point_absc) /* 3. */
			{
			  //printf("C2\n");
			  A += (w_level - control_point_z)*(control_point_absc - fabs(absc0-rb_absc));
			  A += (control_point_absc - fabs(absc0-rb_absc))*(control_point_z - z0)/2.;
			  P_wet += sqrt((control_point_absc - fabs(absc0-rb_absc))*(control_point_absc - fabs(absc0-rb_absc)) + (z0 - control_point_z)*(z0 - control_point_z));
			}
		      else
			{
			  //printf("D2\n");
			  A += (w_level - z0)*fabs(absc0 - w_level_absc)/2.;
			  P_wet += sqrt((w_level - z0)*(w_level - z0) + (absc0 - w_level_absc)*(absc0 - w_level_absc));
			}
		    }
		  else
		    {
		      //printf("E2\n");
		      /*** go to next iter ***/ 
		    }
		}
	    }
	  //printf("A %lf ; P %lf\n",A,P_wet);
	  
	  switch(profile->type) {
	  case ABSC_Z :
	    p0 = p1;
	    p1 = p1->prev;
	    if (p1 != NULL) // Control anti seg. fault
	      {
		absc0 = absc1;
		absc1 = absc1 + fabs(p1->absc - p0->absc);
		z0 = p0->z - height_diff;
		z1 = p1->z - height_diff;
	      }
	    break;
	  case X_Y_Z :
	    q0 = q1;
	    q1 = q1->prev;
	    if (q1 != NULL) // Control anti seg. fault
	      {
		absc0 = absc1;
		absc1 = absc1 + sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y));
		z0 = q0->z - height_diff;
		z1 = q1->z - height_diff;
	      }
	    break;	
	  }
	  
	}
      else
	break;
    }

  /*rslt = interpolation_beyond_right_bank(w_level, dist_from_bank_l, profile, height_diff);
  A = A + rslt[0];
  P_wet = P_wet + rslt[1];
  free(rslt);*/
  wet_param[0] = A;
  wet_param[1] = P_wet;
  return wet_param;
}

double energy_grad_line_slope_TUB(double mean_velocity, double Ks, double Rh43)
{
  double J0;
  J0 = mean_velocity*mean_velocity / (Ks*Ks * Rh43);
  return J0;
}

double left_integrated_velocity_TUB(double *param, double Ks, double J0)
{
  double Rc23, U_l;

  if (fabs(param[1])<EPS2_TUB)
    U_l = 0.;
  else
    {
      Rc23 = pow(param[0]/param[1],2./3.);
      U_l = Ks*Rc23*sqrt(J0);
    }
  
  return U_l;
}

double right_integrated_velocity_TUB(double *param, double Ks, double J0)
{
  double Rc23, U_r;

  if (fabs(param[1])<EPS2_TUB)
    U_r = 0.;
  else
    {
      Rc23 = pow(param[0]/param[1],2./3.);
      U_r = Ks*Rc23*sqrt(J0);
    }
  
  return U_r;
}

double cumulated_flow_rate_from_left_bank_TUB(double U_l, double A_l)
{
  return U_l*A_l;
}

double cumulated_flow_rate_from_right_bank_TUB(double U_r, double A_r)
{
  return U_r*A_r;
}

double profile_filling(double w_level, s_face_hyd *face, s_cross_section_tub *profile, double height_diff) // return the distance from one side to the other
{
  // SW 26/03/2021 height_diff not used
  s_pointAbscZ_hyd *p0, *p1;
  s_pointXYZ_hyd *q0, *q1;
  double absc0, absc1;
  double tot_width;
  int npts;
  
  switch(profile->type)
    {
      // SW 22/03/2021 je n'ai pas bien compris la logique. Pour trouver tot_width (type Absz), tot_width = fabs(face->geometry->p_pointsAbscZ[0]->absc - face->geometry->p_pointsAbscZ[npts - 1]->absc).    
      case ABSC_Z :
      profile->pptsAbscZ = face->geometry->p_pointsAbscZ[0]; 
      p0 = profile->pptsAbscZ;
      while (p0->prev != NULL)
	p0 = p0->prev;
      p1 = p0;
      while (p1->next != NULL)
	p1 = p1->next;
      absc0 = 0.;
      absc1 = fabs(p1->absc - p0->absc);
      break;
      
      /*case ABSC_Z :
      npts =  face->geometry->npts;
      absc0 = face->geometry->p_pointsAbscZ[0]->absc;
      absc1 = face->geometry->p_pointsAbscZ[npts - 1]->absc;
      break;*/
          
    case X_Y_Z :
      profile->pptsXYZ = face->geometry->p_pointsXYZ[0];      
      q0 = profile->pptsXYZ;
      while (q0->prev != NULL)
	q0 = q0->prev;
      q1 = q0;
      while (q1->next != NULL)
	q1 = q1->next;
      absc0 = 0.;
      absc1 = sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y));
      break;
    }

  tot_width = fabs(absc1 - absc0);
  return tot_width;
}

double profile_overflow(double w_level, s_cross_section_tub *profile, double height_diff)
{
  double k, w_absc;
  s_pointAbscZ_hyd *p0, *p1, *new_coordp;
  s_pointXYZ_hyd *q0, *q1, *new_coordq;
  double absc0, absc1, z0, z1;
  double x0_wth, x1_wth, y0_wth, y1_wth;
  double tot_width;

  switch(profile->type)
    {
    case ABSC_Z :
      p0 = profile->pptsAbscZ;
      while (p0->prev != NULL)
	p0 = p0->prev;
      p1 = p0->next;
      absc0 = 0.;
      absc1 = fabs(p1->absc - p0->absc);
      z0 = p0->z - height_diff;
      z1 = p1->z - height_diff;
      x0_wth = p0->absc;
      break;
    
    case X_Y_Z :      
      q0 = profile->pptsXYZ;
      while (q0->prev != NULL)
	q0 = q0->prev;
      q1 = q0->next;
      absc0 = 0.;
      absc1 = sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y));
      z0 = q0->z - height_diff;
      z1 = q1->z - height_diff;
      x0_wth = q0->x;
      y0_wth = q0->y;
      break;
    }

  if (z0 < w_level && fabs(absc1 - absc0) > EPS1_TUB) //left bank test
    {
      if (z0 <= z1)
	printf("Pb with left coord. of river cross-section: there might be infinite interpolation!\n");
      else
	{
	  k = (z1 - z0)/(absc1 - absc0);
	  w_absc = (w_level - z0)/k;
	  printf("k1: %lf\n", k);
	
	  switch(profile->type)
	    {
	    case ABSC_Z :
	      new_coordp = (s_pointAbscZ_hyd *)malloc(sizeof(s_pointAbscZ_hyd));
	      new_coordp->prev = NULL;
	      new_coordp->next = p0;
	      p0->prev = new_coordp;
	      new_coordp->absc = w_absc*(p1->absc - p0->absc)/fabs(p1->absc - p0->absc) + p0->absc;
	      new_coordp->z = w_level + height_diff;
	      x0_wth = new_coordp->absc;
	      break;
	      
	    case X_Y_Z :
	      new_coordq = (s_pointXYZ_hyd *)malloc(sizeof(s_pointXYZ_hyd));
	      new_coordq->prev = NULL;
	      new_coordq->next = q0;
	      q0->prev = new_coordq;
	      new_coordq->x = w_absc*(q1->x - q0->x)/sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y)) + q0->x;
	      new_coordq->y = w_absc*(q1->y - q0->y)/sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y)) + q0->y;
	      new_coordq->z = w_level + height_diff;
	      x0_wth = new_coordq->x;
	      y0_wth = new_coordq->y;
	      break;
	    }
	}
    }

  /*************************************************************/
  
  switch(profile->type)
    {
    case ABSC_Z :
      p0 = profile->pptsAbscZ;
      while (p0->next != NULL)
	p0 = p0->next;
      p1 = p0->prev;
      absc0 = 0.;
      absc1 = fabs(p1->absc - p0->absc);
      z0 = p0->z - height_diff;
      z1 = p1->z - height_diff;
      x1_wth = p0->absc;
      break;
    
    case X_Y_Z :      
      q0 = profile->pptsXYZ;
      while (q0->next != NULL)
	q0 = q0->next;
      q1 = q0->prev;
      absc0 = 0.;
      absc1 = sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y));
      z0 = q0->z - height_diff;
      z1 = q1->z - height_diff;
      x1_wth = q0->x;
      y1_wth = q0->y;
      break;
    }

  if (z0 < w_level && fabs(absc1 - absc0) > EPS1_TUB) //right bank test
    {
      if (z0 <= z1)
	printf("Pb with left coord. of river cross-section: there might be infinite interpolation!\n");
      else
	{
	  k = (z1 - z0)/(absc1 - absc0);
	  w_absc = (w_level - z0)/k;
	  printf("k2: %lf\n", k);
	
	  switch(profile->type)
	    {
	    case ABSC_Z :
	      new_coordp = (s_pointAbscZ_hyd *)malloc(sizeof(s_pointAbscZ_hyd));
	      new_coordp->next = NULL;
	      new_coordp->prev = p0;
	      p0->next = new_coordp;
	      new_coordp->absc = w_absc*(p1->absc - p0->absc)/fabs(p1->absc - p0->absc) + p0->absc;
	      new_coordp->z = w_level + height_diff;
	      x1_wth = new_coordp->absc;
	      break;
	      
	    case X_Y_Z :
	      new_coordq = (s_pointXYZ_hyd *)malloc(sizeof(s_pointXYZ_hyd));
	      new_coordq->next = NULL;
	      new_coordq->prev = q0;
	      q0->next = new_coordq;
	      new_coordq->x = w_absc*(q1->x - q0->x)/sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y)) + q0->x;
	      new_coordq->y = w_absc*(q1->y - q0->y)/sqrt((q1->x-q0->x)*(q1->x-q0->x) + (q1->y-q0->y)*(q1->y-q0->y)) + q0->y;
	      new_coordq->z = w_level + height_diff;
	      x1_wth = new_coordq->x;
	      y1_wth = new_coordq->y;
	      break;
	    }
	}  
    }

  switch(profile->type){
  case ABSC_Z :
    tot_width = fabs(x1_wth - x0_wth);
    printf("x1_wth %lf x0_wth %lf\n", x1_wth, x0_wth);
    break;
  case X_Y_Z :
    tot_width = sqrt((x1_wth - x0_wth)*(x1_wth - x0_wth) + (y1_wth - y0_wth)*(y1_wth - y0_wth));
    printf("x1_wth %lf x0_wth %lf y1_wth %lf y0_wth %lf\n", x1_wth, x0_wth, y1_wth, y0_wth);
    break;
  }

  return tot_width;
}

double equi_flow_rate_distance_TUB(s_cross_section_tub *profile, double height_diff, double tot_width, double U, double w_level, double Ks, double Rh43)
{
  double J0;
  double U_l, U_r, Q_l, Q_r, *param_l, *param_r;
  double delta, dF;
  double Y0, Y1, F0, F1, B0, B1;
  int it = 0;
    
  /*** Algorithme de Broyden ***/
  /* Init */
  Y0 = tot_width/2.;
  B0 = -10.;
  dF = 1.;
  /***********/
  J0 = energy_grad_line_slope_TUB(U, Ks, Rh43);
  param_l = wet_param_from_left_bank_TUB(w_level, Y0, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
  param_r = wet_param_from_right_bank_TUB(w_level, Y0, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
  U_l = left_integrated_velocity_TUB(param_l, Ks, J0);
  U_r = right_integrated_velocity_TUB(param_r, Ks, J0);
  Q_l = cumulated_flow_rate_from_left_bank_TUB(U_l, param_l[0]);
  Q_r = cumulated_flow_rate_from_right_bank_TUB(U_r, param_r[0]);
  
  
  /* SW 31/30/2021 need to free param_l and param_r which is allocated in wet_param_from_left_bank_TUB and wet_param_from_right_bank_TUB. If not, memory leak */
  free(param_l);
  param_l = NULL;
  free(param_r);
  param_r = NULL;
  
  /***********/
  F0 = Q_l - Q_r;
  /* Boucle */
  while (fabs(F0) > EPS1_TUB)
    {
      delta = -(F0/B0);
      Y1 = Y0 + delta;
      /************************************************/
      param_l = wet_param_from_left_bank_TUB(w_level, Y1, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
      param_r = wet_param_from_right_bank_TUB(w_level, Y1, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
      U_l = left_integrated_velocity_TUB(param_l, Ks, J0);
      U_r = right_integrated_velocity_TUB(param_r, Ks, J0);
      Q_l = cumulated_flow_rate_from_left_bank_TUB(U_l, param_l[0]);
      Q_r = cumulated_flow_rate_from_right_bank_TUB(U_r, param_r[0]);
      
      /* SW 31/30/2021 need to free param_l and param_r which is allocated in wet_param_from_left_bank_TUB and wet_param_from_right_bank_TUB. If not, memory leak */
      free(param_l);
      param_l = NULL;
      free(param_r);
      param_r = NULL;
      
      /************************************************/
      F1 = Q_l - Q_r;
      dF = F1 - F0;
      B1 = B0 + (dF - B0*delta)/delta;
      /* réaffectation des paramètres de l'algo */
      Y0 = Y1;
      F0 = F1;
      B0 = B1;
      //printf("delta: %lf, dF: %lf, rslt algo: %lf, Ql: %lf, Qr: %lf\n",delta,dF,Y0, Q_l, Q_r); 
      it++;
      if (it == DIV_PARAM)
	break;
    }
  
  /*** Algo de secours ***/
  if(Y0<=0.2*tot_width || Y0>=tot_width*0.8 || fabs(F0) > EPS1_TUB || Y0 != Y0)
    {
      //printf("largeur totale %lf milieu débit %lf Heau %lf Hdiff %lf F0 %lf\n",tot_width,Y0,w_level,height_diff,F0);
      printf("THERE HAD BEEN OR MIGHT HAVE BEEN SOME TROUBLE WITH MID_FLOW_RATE CALCULATION -> SWITCHING TO SIMPLER ALGO.\n");
      Y0 = mid_flow_back_up_algorithm(tot_width, w_level, profile, Ks, J0, height_diff);
      printf("WARNING: ");
      printf("largeur totale %lf milieu débit %lf Heau %lf Hdiff %lf\n",tot_width,Y0,w_level,height_diff);
    }
  
  /*** Fin de l'algo ***/
  //free(profile);
  return Y0;
}

double mid_flow_back_up_algorithm(double tot_width, double w_level, s_cross_section_tub *profile, double Ks, double J0, double height_diff)
{
  double Y0, Y1, F1, mid;
  double *param_l, *param_r;
  double U_l, Q_l, U_r, Q_r;
  
  /* Init. */
  Y0 = 0.;
  Y1 = tot_width;
  F1 = 1.;
  /* Algo. */
  while (fabs(F1) > EPS1_TUB)
    {
      mid = (Y0 + Y1)/2.;
      /***********/
      param_l = wet_param_from_left_bank_TUB(w_level, mid, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
      U_l = left_integrated_velocity_TUB(param_l, Ks, J0);
      Q_l = cumulated_flow_rate_from_left_bank_TUB(U_l, param_l[0]);
      
      /***********/
      param_r = wet_param_from_right_bank_TUB(w_level, mid, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
      U_r = right_integrated_velocity_TUB(param_r, Ks, J0);
      Q_r = cumulated_flow_rate_from_right_bank_TUB(U_r, param_r[0]);
      /***********/
      
      /* SW 31/30/2021 need to free param_l and param_r which is allocated in wet_param_from_left_bank_TUB and wet_param_from_right_bank_TUB. If not, memory leak */
      free(param_l);
      param_l = NULL;
      free(param_r);
      param_r = NULL;
      
      F1 = Q_r - Q_l;
      if (F1 > 0.)
	Y0 = mid;
      else
	Y1 = mid;
      printf("Y0 %lf Y1 %lf F1 %lf\n", Y0, Y1, F1);
    }
  /* Results */
  return Y0;
}
