/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libtube
* FILE NAME: tube_outputs.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
* Possibility to parameterize the number of tubes locally.
*
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2023 Contributors to the libtube library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"

#include "HYD.h"
#include "TUB.h"

void write_wtk_file_for_sig_TUB(s_inout_set_io *pinout, double time, s_chyd *pchyd, s_chronos_CHR *chronos, s_def_tub ****tube, FILE *fp)
{
  char file_path[STRING_LENGTH_LP], dir_path[STRING_LENGTH_LP];
  char command1[STRING_LENGTH_LP], command2[STRING_LENGTH_LP];
  FILE *wtk_file;
  int r, e, t;

  sprintf(dir_path,"%s/tube",pinout->name_outputs);
  sprintf(file_path,"%s/geotube%.0lf.txt",dir_path,time/(24.*3600.));
  
  if (time < EPS1_TUB + chronos->t[BEGINNING_CHR])
    {
      sprintf(command1,"mkdir %s", dir_path);
      system(command1);
      sprintf(command2,"rm -rf %s/*", dir_path);
      system(command2);
    }
  if ((wtk_file = fopen(file_path,"w")) == 0)
    LP_error(fp,"Impossible to open tube wtk file\n");
  
  fprintf(wtk_file,"ID_TUBE;CELL_CENTRE;H_WATER;U_WATER\n");
  for (r = 0; r < pchyd->counter->nreaches; r++)
    for (e = 0; e < pchyd->p_reach[r]->nele; e++)
      for (t = 0; t < pchyd->p_reach[r]->n_tube; t++)
	{
	  if (tube[r][e][t]->geocoords != NULL)
	    {
	      if (error_geocoord_TUB(tube[r][e][t]->geocoords) == 1)
		{
		  printf("Error with tube[%d][%d][%d] of abs. id: %d, some coords are similar.\n",r,e,t,tube[r][e][t]->id_tube_abs);
		  printf("%lf %lf, %lf %lf, %lf %lf, %lf %lf\n", tube[r][e][t]->geocoords->vertices[LEFT_UP][0], tube[r][e][t]->geocoords->vertices[LEFT_UP][1], tube[r][e][t]->geocoords->vertices[RIGHT_UP][0], tube[r][e][t]->geocoords->vertices[RIGHT_UP][1], tube[r][e][t]->geocoords->vertices[RIGHT_DOWN][0], tube[r][e][t]->geocoords->vertices[RIGHT_DOWN][1], tube[r][e][t]->geocoords->vertices[LEFT_DOWN][0], tube[r][e][t]->geocoords->vertices[LEFT_DOWN][1]);
		  break;
		}
	      else
		{
		  fprintf(wtk_file,"%d;POINT(%lf %lf);%lf;%lf\n", tube[r][e][t]->id_tube_abs, tube[r][e][t]->geocoords->centre[0], tube[r][e][t]->geocoords->centre[1], (tube[r][e][t]->param_amont->H + tube[r][e][t]->param_aval->H)/2., (tube[r][e][t]->param_amont->U + tube[r][e][t]->param_aval->U)/2.);
		}
	    }
	}
  fclose(wtk_file);
}

void time_geo_outputs_TUB(s_inout_set_io *pinout, double time, s_chyd *pchyd, s_def_tub ****tube, FILE *fp)
{
  char dir_path[STRING_LENGTH_LP], file_path[STRING_LENGTH_LP], file_path0[STRING_LENGTH_LP];
  char command1[STRING_LENGTH_LP], command2[STRING_LENGTH_LP];
  FILE *wtk_file, *time_file;
  int r, e, t, tbis, i;
  int file_opened = 0;

  sprintf(dir_path,"%s/tube",pinout->name_outputs);
  sprintf(file_path0,"%s/coord_geotube.txt",dir_path);
  sprintf(file_path,"%s/time_geotube_%.0lf.txt",dir_path,time/(24.*3600.));

  if (time < EPS_HYD)
    {
      sprintf(command1,"mkdir %s", dir_path);
      system(command1);
      sprintf(command2,"rm -rf %s/*", dir_path);
      system(command2);
      if ((wtk_file = fopen(file_path0,"w")) == 0)
	{
	  LP_error(fp,"Impossible to open coord_geotube file\n");
	  file_opened = 1;
	}
      if ((time_file = fopen(file_path,"w")) == 0)
	LP_error(fp,"Impossible to open time_geotube wtk file\n");

      fprintf(wtk_file,"ID_TUBE;CELL_CENTRE;CELL_VERTICES\n");
      fprintf(time_file,"TIME;H_WATER;U_WATER;ALTI_WATER\n");
    }
  else
    if ((time_file = fopen(file_path,"a")) == 0)
      LP_error(fp,"Impossible to open time_geotube wtk file\n");
  
  for (r = 0; r < pchyd->counter->nreaches; r++)
    for (e = 0; e < pchyd->p_reach[r]->nele; e++)
      for (t = 0; t < pchyd->p_reach[r]->n_tube; t++)
	{
	  if (tube[r][e][t]->geocoords != NULL)
	    {
	      if (error_geocoord_TUB(tube[r][e][t]->geocoords) == 1)
		{
		  printf("Error with tube[%d][%d][%d] of abs. id: %d, some coords are similar.\n",r,e,t,tube[r][e][t]->id_tube_abs);
		  /*printf("Destruction of the coresponding tubes' series.\n");
		  for (tbis=0; tbis<pchyd->p_reach[r]->n_tube; tbis++)
		  {
		  for (tbis=0; tbis<pchyd->p_reach[r]->n_tube; tbis++)
		  {
		  for (i=0; i<N_VERTEX; i++)
		  free(tube[r][e][tbis]->geocoords->vertices[i]);
		  free(tube[r][e][tbis]->geocoords->centre);
		  free(tube[r][e][tbis]->geocoords);
		  }
		  break;
		  }*/
		}
	      else
		{
		  if (time < EPS_HYD)
		    fprintf(wtk_file,"%d;POINT(%lf %lf);POLYGON((%lf %lf, %lf %lf, %lf %lf, %lf %lf, %lf %lf))\n", tube[r][e][t]->id_tube_abs, tube[r][e][t]->geocoords->centre[0], tube[r][e][t]->geocoords->centre[1], tube[r][e][t]->geocoords->vertices[LEFT_UP][0], tube[r][e][t]->geocoords->vertices[LEFT_UP][1], tube[r][e][t]->geocoords->vertices[RIGHT_UP][0], tube[r][e][t]->geocoords->vertices[RIGHT_UP][1], tube[r][e][t]->geocoords->vertices[RIGHT_DOWN][0], tube[r][e][t]->geocoords->vertices[RIGHT_DOWN][1], tube[r][e][t]->geocoords->vertices[LEFT_DOWN][0], tube[r][e][t]->geocoords->vertices[LEFT_DOWN][1], tube[r][e][t]->geocoords->vertices[LEFT_UP][0], tube[r][e][t]->geocoords->vertices[LEFT_UP][1]);

		  fprintf(time_file,"%lf;%d;%lf;%lf;%lf\n", time, tube[r][e][t]->id_tube_abs, (tube[r][e][t]->param_amont->H + tube[r][e][t]->param_aval->H)/2., (tube[r][e][t]->param_amont->U + tube[r][e][t]->param_aval->U)/2., pchyd->p_reach[r]->p_ele[e]->center->hydro->Z[T_HYD]);
		}
	    }
	}
  if (file_opened == 1)
    fclose(wtk_file);
  fclose(time_file);
}

// SW 30/20/2021 chain output pointers
/** \fn s_output_tube_type  *chain_output_bck_TUB(s_output_tube_type  *pout_tube1, s_output_tube_type  *pout_tube2)
 * \brief Links two elements with structure s_output_tube_type bck, ie usefull for yacc for undetermined list
 *
 * Read the pile from top (end of the list) to bottom (beginning of the list), but return a pointer to the beginning of the list as a first element
*/
s_output_tube_type  *TUB_chain_output_bck_tube(s_output_tube_type  *pout_tube1, s_output_tube_type  *pout_tube2)
{
   pout_tube1->next = pout_tube2;
   pout_tube2->prev = pout_tube1;

   return pout_tube1;
}

