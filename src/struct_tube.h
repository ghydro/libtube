/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libtube
* FILE NAME: struct_tube.h
* BRANCH NAME: main
* 
* CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
* Possibility to parameterize the number of tubes locally.
*
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2023 Contributors to the libtube library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/
#ifndef _STRUCT_TUBE_H_
#define _STRUCT_TUBE_H_

typedef struct tube_def s_def_tub;
typedef struct cross_section s_cross_section_tub;
typedef struct tube_param s_tube_param_tub;
typedef struct tube_bief_voisin s_reach_connect_tub;
typedef struct tube_coord s_georef_tub;
typedef struct tube_ouput s_output_tube;
typedef struct tube_ouput_type s_output_tube_type;

/********************************/

struct tube_def{
  int id_tube_abs;
  s_tube_param_tub *param_amont;
  s_tube_param_tub *param_aval;
  s_def_tub *voisin[N_NEIGH_DIR]; // NULL if reach ends
  s_reach_connect_tub *connect_bief; // structure of substitution in case of reach boundarie is reached
  s_georef_tub *geocoords;
};

struct cross_section{
  int type; /* Type : ABSC_Z or X_Y_Z */
  s_pointAbscZ_hyd *pptsAbscZ;
  s_pointXYZ_hyd *pptsXYZ;
  int n_tube;
  double **p_coord_tube; /* From left bank to the right one, first dim = n_tube, second dim = 2, coord. given in terms of distance from the initial bank */
  double Q; // flow rate over the cross-section
  double w_level;
  double J0; // energy gradient line slope
};

struct tube_param{
  double *pcoord; // dim = 2, points to p_coord_tube
  double A; // surface of one tube extremity
  double U; // stream velocity
  double H; // water height from river bed 
  double U_star; // shear velocity at river bottom
  double dy; // tube width
};

struct tube_bief_voisin{
  int n_neigh_tubes;
  int type; // UPSTREAM or DOWNSTREAM
  s_def_tub **neigh_tubes; // dim = n_neigh_tubes
  double *frac_neigh_tubes; // ..., represents the fraction of flow rate given by a neighbour tube
};

struct tube_coord{
  double *vertices[N_VERTEX]; // georef. coord. of the 4 extremities a tube (dim = 4 x 2)
  double *centre; // georef. coord. of the centre of a tube (dim = 2)
};

/* SW 3003/2021 add output struct */
struct tube_ouput_type {
   /* output type TUBE_MESH or TUBE_HYD*/
   int output_type;
   double t_out[NTIME_IO];
   double dt;
   char river[STRING_LENGTH_LP];
   /* pk to print output*/
   s_lp_pk_hyd *pk_tube;
   double time_unit;
   s_output_tube_type *prev;
   s_output_tube_type *next;
};

struct tube_ouput {
   int calc_output;
   /* number of ouputs*/
   int nout;
   s_output_tube_type **poutput_tube_type;
};

#define new_tube_output_type() ((s_output_tube_type *) malloc(sizeof(s_output_tube_type))) 
#define new_tube_out_put() ((s_output_tube *) malloc(sizeof(s_output_tube)))
#endif
