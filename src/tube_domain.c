/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libtube
* FILE NAME: tube_domain.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
* Possibility to parameterize the number of tubes locally.
*
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2023 Contributors to the libtube library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"

#include "HYD.h"
#include "TUB.h"


s_def_tub ***build_tube_domain_per_reach_TUB(s_reach_hyd *reach, int *id_t) // This is a partnership with libhyd®
{
  int e, t;
  int n_tube;
  s_face_hyd **face;
  s_def_tub ***tube;
  s_cross_section_tub **profile;
  s_tube_param_tub ***param;
  s_element_hyd *pele;

  //face = (s_face_hyd **)malloc(sizeof(s_face_hyd *)*2);
  profile = (s_cross_section_tub **)malloc(sizeof(s_cross_section_tub *)*2);
  param = (s_tube_param_tub ***)malloc(sizeof(s_tube_param_tub **)*2);    
  n_tube = reach->n_tube;
  tube = (s_def_tub ***)malloc(sizeof(s_def_tub**)*reach->nele);
  
  /*** I. First (upstream) tubes of the reach ***/
  e = 0;
  tube[e] = (s_def_tub **)malloc(sizeof(s_def_tub*)*n_tube);
  pele = reach->p_ele[e];
  face = pele->face[X_HYD];
  profile[0] = profile_building_TUB(face[0], n_tube);
  param[0] = tubes_param_over_one_face_TUB(profile[0]);
  for (t=0; t<n_tube; t++)
    {
      tube[e][t] = (s_def_tub *)malloc(sizeof(s_def_tub));
      tube[e][t]->id_tube_abs = *id_t;
      (*id_t)++;
      tube[e][t]->param_amont = param[0][t];
      
      if (t == 0)
	tube[e][t]->voisin[LEFT_TUB] = NULL;
      else if (t == n_tube - 1)
	{
	  tube[e][t]->voisin[RIGHT_TUB] = NULL;
	  tube[e][t]->voisin[LEFT_TUB] = tube[e][t-1];
	  tube[e][t-1]->voisin[RIGHT_TUB] = tube[e][t];
	}
      else
	{
	  tube[e][t]->voisin[LEFT_TUB] = tube[e][t-1];
	  tube[e][t-1]->voisin[RIGHT_TUB] = tube[e][t];
	}

      tube[e][t]->voisin[UPWARD_TUB] = NULL;
    }
  up_georeferencing_TUB(tube[e], n_tube, profile[0]); 
  free_profile_TUB(profile[0]);
    
  /*** II. Next tubes ***/
  for(e=1; e<reach->nele-1; e++)
    {
      if (reach->id[ABS_HYD] == 71 && e == 32)
	printf("%d\n",e);
      
      tube[e] = (s_def_tub **)malloc(sizeof(s_def_tub*)*n_tube);
      pele = reach->p_ele[e];
      face = pele->face[X_HYD];
      profile[0] = profile_building_TUB(face[0], n_tube);
      param[0] = tubes_param_over_one_face_TUB(profile[0]);
      for (t=0; t<n_tube; t++)
	{
	  tube[e][t] = (s_def_tub *)malloc(sizeof(s_def_tub));
	  tube[e][t]->id_tube_abs = *id_t;
	  (*id_t)++;
	  tube[e][t]->param_amont = param[0][t];	  
	  tube[e-1][t]->param_aval = tube[e][t]->param_amont;
	  
	  if (t == 0)
	    tube[e][t]->voisin[LEFT_TUB] = NULL;
	  else if (t == n_tube - 1)
	    {
	      tube[e][t]->voisin[RIGHT_TUB] = NULL;
	      tube[e][t]->voisin[LEFT_TUB] = tube[e][t-1];
	      tube[e][t-1]->voisin[RIGHT_TUB] = tube[e][t];
	    }
	  else
	    {
	      tube[e][t]->voisin[LEFT_TUB] = tube[e][t-1];
	      tube[e][t-1]->voisin[RIGHT_TUB] = tube[e][t];
	    }

	  tube[e][t]->voisin[UPWARD_TUB] = tube[e-1][t];
	  tube[e-1][t]->voisin[DOWNWARD_TUB] = tube[e][t];
	}
      up_georeferencing_TUB(tube[e], n_tube, profile[0]);
      if(tube[e-1][0]->geocoords != NULL)
	down_georeferencing_TUB(tube[e-1], n_tube, profile[0]);
      free_profile_TUB(profile[0]);
    }

  /*** III. Last (downstream) tubes of the reach ***/
  tube[e] = (s_def_tub **)malloc(sizeof(s_def_tub*)*n_tube);
  pele = reach->p_ele[e];
  face = pele->face[X_HYD];
  profile[0] = profile_building_TUB(face[0], n_tube);
  /*** If the last face of the reach has its dx = 0, then geometry is not define... ***/
  if ((face[1]->geometry->type == ABSC_Z && face[1]->geometry->p_pointsAbscZ == NULL) || (face[1]->geometry->type == X_Y_Z && face[1]->geometry->p_pointsXYZ == NULL))
    profile[1] = backup_profile_building_TUB(face, n_tube);
  else
    profile[1] = profile_building_TUB(face[1], n_tube);
  /************************************************************************************/
  param[0] = tubes_param_over_one_face_TUB(profile[0]);
  param[1] = tubes_param_over_one_face_TUB(profile[1]);
  for (t=0; t<n_tube; t++)
    {
      tube[e][t] = (s_def_tub *)malloc(sizeof(s_def_tub));
      tube[e][t]->id_tube_abs = *id_t;
      (*id_t)++;
      tube[e][t]->param_amont = param[0][t];
      tube[e][t]->param_aval = param[1][t];
      tube[e-1][t]->param_aval = tube[e][t]->param_amont;
      
      if (t == 0)
	tube[e][t]->voisin[LEFT_TUB] = NULL;
      else if (t == n_tube - 1)
	{
	  tube[e][t]->voisin[RIGHT_TUB] = NULL;
	  tube[e][t]->voisin[LEFT_TUB] = tube[e][t-1];
	  tube[e][t-1]->voisin[RIGHT_TUB] = tube[e][t];
	}
      else
	{
	  tube[e][t]->voisin[LEFT_TUB] = tube[e][t-1];
	  tube[e][t-1]->voisin[RIGHT_TUB] = tube[e][t];
	}

      tube[e][t]->voisin[DOWNWARD_TUB] = NULL;
      tube[e][t]->voisin[UPWARD_TUB] = tube[e-1][t];
      tube[e-1][t]->voisin[DOWNWARD_TUB] = tube[e][t];
    }
  if(tube[e-1][0]->geocoords != NULL)
    down_georeferencing_TUB(tube[e-1], n_tube, profile[0]);
  if ((face[1]->geometry->type == X_Y_Z && face[1]->geometry->p_pointsXYZ == NULL) || face[0]->geometry->type == ABSC_Z || face[1]->geometry->type == ABSC_Z)
    for (t=0; t<n_tube; t++)
      tube[e][t]->geocoords = NULL;
  else
    {
      up_georeferencing_TUB(tube[e], n_tube, profile[0]);
      down_georeferencing_TUB(tube[e], n_tube, profile[1]);
    }
  free_profile_TUB(profile[0]);
  free_profile_TUB(profile[1]);
  free(profile);
  
  return tube;
}

s_def_tub ****build_tube_domain_for_all_reaches_TUB(s_chyd *chyd, int ntube_user)
{
  int r, n_reach, id_t = 0;
  s_def_tub ****tube;
  s_reach_hyd *reach;
  
  n_reach = chyd->counter->nreaches;
  tube = (s_def_tub ****)malloc(sizeof(s_def_tub***)*n_reach);
  for (r=0; r<n_reach; r++)
    {
      reach = chyd->p_reach[r];
      reach->n_tube = ntube_user;
    }
  reduce_tube_number(chyd); // optional: allows to have less tubes into thin reaches
  for (r=0; r<n_reach; r++)
    {
      reach = chyd->p_reach[r];
      tube[r] = build_tube_domain_per_reach_TUB(reach, &id_t);
      printf("REACH %d DONE\n",r);
    }

  return tube;
}

void build_singularities_linkage_TUB(s_chyd *chyd, s_def_tub ****tube)
{
  int s, r, n_sing, n_reach, br, brup;
  int t, n_tube, tup, n_tubeup, td, inc, i, t1, t2, indicator, nnt;
  int *r_reach_up, *r_reach_down;
  s_def_tub ***tubes_up, ***tubes_down;
  s_singularity_hyd *sing;
  double Qtot, Qup, Qdown, Qup_abs, Qdown_abs, Qprev;

  printf("LINKING TUBES AT SINGULARITIES...\n");
  
  n_sing = chyd->counter->nsing;
  n_reach = chyd->counter->nreaches;
  for (s=0; s<n_sing; s++)
    {
      sing = chyd->p_sing[s];
      r_reach_up = (int *)malloc(sizeof(int)*sing->nupstr_reaches);
      r_reach_down = (int *)malloc(sizeof(int)*sing->ndownstr_reaches);
      tubes_up = (s_def_tub ***)malloc(sizeof(s_def_tub**)*sing->nupstr_reaches);
      tubes_down = (s_def_tub ***)malloc(sizeof(s_def_tub**)*sing->ndownstr_reaches);
      /*/ Relating "abs. id" to "loc. id" for the reaches beside the sing. /*/
      for (r=0; r<n_reach; r++)
	  {
	    for (br=0; br<sing->nupstr_reaches; br++)
	      if (chyd->p_reach[r] == sing->p_reach[UPSTREAM][br])
		r_reach_up[br] = r;
	    for (br=0; br<sing->ndownstr_reaches; br++)
	      if (chyd->p_reach[r] == sing->p_reach[DOWNSTREAM][br])
		r_reach_down[br] = r;
	  }
      /*/ Associating the right tubes, upward and downward, beside the sing. /*/
      for (br=0; br<sing->nupstr_reaches; br++)
	tubes_up[br] = tube[r_reach_up[br]][chyd->p_reach[r_reach_up[br]]->nele-1];
      for (br=0; br<sing->ndownstr_reaches; br++)
	tubes_down[br] = tube[r_reach_down[br]][0];
      /*** Init. total flow rate over sing. ***/
      Qtot = 0.;
      if (sing->type == DISCHARGE)
	Qtot = tubes_down[0][0]->param_aval->A * tubes_down[0][0]->param_aval->U * chyd->p_reach[r_reach_down[0]]->n_tube;
      else
	for (br=0; br<sing->nupstr_reaches; br++)
	  {
	    printf("A %lf U %lf n_tube %d\n",tubes_up[br][0]->param_aval->A,tubes_up[br][0]->param_aval->U,chyd->p_reach[r_reach_up[br]]->n_tube);
	    Qtot = Qtot + tubes_up[br][0]->param_aval->A * tubes_up[br][0]->param_aval->U * chyd->p_reach[r_reach_up[br]]->n_tube;  
	  }
      printf("Qbulk %lf\n",Qtot);
      printf("Entering %s case...\n", HYD_name_sing_type(sing->type));
      
      switch(sing->type){
	
	/********************************************************************/
	/********************** I. MONO REACH CASES *************************/
	/********************************************************************/
      case HYDWORK:  
      case CONTINU:
	br = 0;
	/*** Same number of tubes from one reach to another ***/ 
	if (chyd->p_reach[r_reach_up[br]]->n_tube == chyd->p_reach[r_reach_down[br]]->n_tube) 
	  {
	    n_tube = chyd->p_reach[r_reach_up[br]]->n_tube;
	    for (t=0; t<n_tube; t++)
	      if (tubes_up[br][t]->voisin[DOWNWARD_TUB] == NULL && tubes_down[br][t]->voisin[UPWARD_TUB] == NULL)
		{
		  tubes_up[br][t]->voisin[DOWNWARD_TUB] = tubes_down[br][t];
		  tubes_down[br][t]->voisin[UPWARD_TUB] = tubes_up[br][t];
		}
	      else
		{
		  LP_error(stderr,"Impossible: ending tubes of reach cannot already be allocated. Quitting program.\n");
		  exit(0);
		}
	    break;
	  }
	
	/*** Different number of tubes from one reach to another ***/
	else
	  {
	    /*** Test to check whether tubes are allocated or not.
		 Tubes are not supposed to be allocated at ends of reaches. ***/
	    n_tubeup = chyd->p_reach[r_reach_up[br]]->n_tube; // upward reach
	    for (t=0; t<n_tubeup; t++)
	      if (tubes_up[br][t]->voisin[DOWNWARD_TUB] != NULL)
		{
		  LP_error(stderr,"Impossible: ending tubes of reach cannot already be allocated. Quitting program.\n");
		  exit(0);
		}
	    
	    n_tube = chyd->p_reach[r_reach_down[br]]->n_tube; // downward reach
	    for (t=0; t<n_tube; t++)
	      if (tubes_down[br][t]->voisin[UPWARD_TUB] != NULL)
		{
		  LP_error(stderr,"Impossible: ending tubes of reach cannot already be allocated. Quitting program.\n");
		  exit(0);
		}
	    
	    if (n_tubeup > n_tube) /*/ I. More tubes upward than downward /*/ /******************************************************************/
	      {
		/****/ /* 1. Treatment of upward tubes */
		td = 0; tup = 0;
		Qdown_abs = tubes_down[br][td]->param_amont->A * tubes_down[br][td]->param_amont->U; Qup_abs = tubes_up[br][tup]->param_aval->A * tubes_up[br][tup]->param_aval->U;
		Qdown = Qdown_abs; Qup = Qup_abs;
		/****/
		printf("\nup %lf dwn %lf\n", Qup_abs, Qdown_abs);	
		while (tup < n_tubeup && td < n_tube)
		  {
		    printf("%lf %lf\n",Qup,Qdown);
		    if (Qdown >= Qup || fabs(Qup - Qdown) < EPS2_TUB)
		      {
			tubes_up[br][tup]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
			tubes_up[br][tup]->connect_bief->type = DOWNSTREAM;
			tubes_up[br][tup]->connect_bief->n_neigh_tubes = 1;
			tubes_up[br][tup]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*));
			tubes_up[br][tup]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double));			
			tubes_up[br][tup]->connect_bief->neigh_tubes[0] = tubes_down[br][td];		  
			tubes_up[br][tup]->connect_bief->frac_neigh_tubes[0] = 1.;
			printf("tube up %d tube d neigh %d frac %lf\n", tup, td, tubes_up[br][tup]->connect_bief->frac_neigh_tubes[0]);
			if (fabs(Qdown - Qup) < EPS2_TUB)
			  {
			    td++;
			    Qdown = Qdown + Qdown_abs;
			  }
			/****/
			tup++;
			Qup = Qup + Qup_abs;
			/****/
		      }
		    else
		      {
			tubes_up[br][tup]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
			tubes_up[br][tup]->connect_bief->type = DOWNSTREAM;
			tubes_up[br][tup]->connect_bief->n_neigh_tubes = 2;
			tubes_up[br][tup]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*)*2);
			tubes_up[br][tup]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double)*2);
			tubes_up[br][tup]->connect_bief->neigh_tubes[0] = tubes_down[br][td];		  
			tubes_up[br][tup]->connect_bief->frac_neigh_tubes[0] = 1. - ((Qup - Qdown) / Qup_abs);
			printf("tube up %d tube d neigh %d frac %lf\n", tup, td, tubes_up[br][tup]->connect_bief->frac_neigh_tubes[0]);
			td++;
			tubes_up[br][tup]->connect_bief->neigh_tubes[1] = tubes_down[br][td];		  
			tubes_up[br][tup]->connect_bief->frac_neigh_tubes[1] = (Qup - Qdown) / Qup_abs;
			printf("tube up %d tube d neigh %d frac %lf\n", tup, td, tubes_up[br][tup]->connect_bief->frac_neigh_tubes[1]);
			/****/
			tup++;
			Qup = Qup + Qup_abs;
			Qdown = Qdown + Qdown_abs;
			/****/
		      }
		  }
		
		/****/ /* 2. Treatment of downward tubes */
		td = 0; tup = 0; inc = 1;
		Qdown_abs = tubes_down[br][td]->param_amont->A*tubes_down[br][td]->param_amont->U; Qup_abs = tubes_up[br][tup]->param_aval->A*tubes_up[br][tup]->param_aval->U;
		Qdown = Qdown_abs; Qup = Qup_abs;
		/****/
		printf("\nup %lf dwn %lf\n", Qup_abs, Qdown_abs);
		while (tup < n_tubeup && td < n_tube)
		  {
		    printf("%lf %lf\n",Qup,Qdown);
		    if (Qdown >= Qup || fabs(Qdown - Qup) < EPS2_TUB)
		      {
			if (fabs(Qdown - Qup) < EPS2_TUB)
			  {
			    tubes_down[br][td]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
			    tubes_down[br][td]->connect_bief->type = UPSTREAM;
			    tubes_down[br][td]->connect_bief->n_neigh_tubes = inc;
			    tubes_down[br][td]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*)*inc);
			    tubes_down[br][td]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double)*inc);
			    Qup = Qup - inc*Qup_abs;
			    Qdown = Qdown - Qdown_abs;
			    Qprev = Qdown;
			    for (i=0; i<inc-1; i++)
			      {
				Qup = Qup + Qup_abs;
				tubes_down[br][td]->connect_bief->neigh_tubes[i] = tubes_up[br][tup-(inc-(i+1))];
				tubes_down[br][td]->connect_bief->frac_neigh_tubes[i] = (Qup - Qprev) / Qdown_abs;
				printf("tube down %d tube up neigh %d frac %lf\n", td, tup-(inc-(i+1)), tubes_down[br][td]->connect_bief->frac_neigh_tubes[i]);
				Qprev = Qup;
			      }
			    Qdown = Qdown + Qdown_abs;
			    tubes_down[br][td]->connect_bief->neigh_tubes[i] = tubes_up[br][tup-(inc-(i+1))]; // definition of the last tube of a series
			    tubes_down[br][td]->connect_bief->frac_neigh_tubes[i] = (Qdown - Qup) / Qdown_abs;
			    printf("tube down %d tube up neigh %d frac %lf\n", td, tup-(inc-(i+1)), tubes_down[br][td]->connect_bief->frac_neigh_tubes[i]);
			    /****/
			    td++;
			    Qup = Qup + Qup_abs;
			    Qdown = Qdown + Qdown_abs;
			    inc = 0;
			    /****/ 
			  }
			else
			  {
			    Qup = Qup + Qup_abs;
			    tup++;
			    inc++;
			  }
		      }
		    else
		      {
			tubes_down[br][td]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
			tubes_down[br][td]->connect_bief->type = UPSTREAM;
			tubes_down[br][td]->connect_bief->n_neigh_tubes = inc;
			tubes_down[br][td]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*)*inc);
			tubes_down[br][td]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double)*inc);
			Qup = Qup - inc*Qup_abs;
			Qdown = Qdown - Qdown_abs;
			Qprev = Qdown;
			for (i=0; i<inc-1; i++)
			  {
			    Qup = Qup + Qup_abs;
			    tubes_down[br][td]->connect_bief->neigh_tubes[i] = tubes_up[br][tup-(inc-(i+1))];
			    tubes_down[br][td]->connect_bief->frac_neigh_tubes[i] = (Qup - Qprev) / Qdown_abs;
			    printf("tube down %d tube up neigh %d frac %lf\n", td, tup-(inc-(i+1)), tubes_down[br][td]->connect_bief->frac_neigh_tubes[i]);
			    Qprev = Qup;
			  }
			Qdown = Qdown + Qdown_abs;
			tubes_down[br][td]->connect_bief->neigh_tubes[i] = tubes_up[br][tup-(inc-(i+1))]; // definition of the last tube of a series
			tubes_down[br][td]->connect_bief->frac_neigh_tubes[i] = (Qdown - Qup) / Qdown_abs;
			printf("tube down %d tube up neigh %d frac %lf\n", td, tup-(inc-(i+1)), tubes_down[br][td]->connect_bief->frac_neigh_tubes[i]);
			/****/
			td++;
			Qup = Qup + Qup_abs;
			Qdown = Qdown + Qdown_abs;
			inc = 1;
			/****/
		      }
		  }
	      }
	    else /*/ II. Less tubes upward than downward /*/ /********************************************************/
	      {
		/****/
		td = 0; tup = 0;
		Qdown_abs = tubes_down[br][td]->param_amont->A*tubes_down[br][td]->param_amont->U; Qup_abs = tubes_up[br][tup]->param_aval->A*tubes_up[br][tup]->param_aval->U;
		Qdown = Qdown_abs; Qup = Qup_abs;
		/****/
		printf("\nup %lf dwn %lf\n", Qup_abs, Qdown_abs);
		while (tup < n_tubeup && td < n_tube) /// 1. Treatment of downward tubes (when they are more numerous)
		  {
		    printf("%lf %lf\n",Qup,Qdown); 
		    if (Qup >= Qdown || fabs(Qup - Qdown) < EPS2_TUB) // Staying in the same opposite upward tube //
		      {
			tubes_down[br][td]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
			tubes_down[br][td]->connect_bief->type = UPSTREAM;
			tubes_down[br][td]->connect_bief->n_neigh_tubes = 1;
			tubes_down[br][td]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*));
			tubes_down[br][td]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double));			
			tubes_down[br][td]->connect_bief->neigh_tubes[0] = tubes_up[br][tup];		  
			tubes_down[br][td]->connect_bief->frac_neigh_tubes[0] = 1.;
			printf("tube d %d tube up neigh %d frac %lf\n", td, tup, tubes_down[br][td]->connect_bief->frac_neigh_tubes[0]);
			if (fabs(Qdown - Qup) < EPS2_TUB)
			  {
			    tup++;
			    Qup = Qup + Qup_abs;
			  }	
			/****/
			td++;
			Qdown = Qdown + Qdown_abs;
			/****/
		      }
		    else // Switching to the next opposite upward tube //
		      {
			tubes_down[br][td]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
			tubes_down[br][td]->connect_bief->type = UPSTREAM;
			tubes_down[br][td]->connect_bief->n_neigh_tubes = 2;
			tubes_down[br][td]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*)*2);
			tubes_down[br][td]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double)*2);
			tubes_down[br][td]->connect_bief->neigh_tubes[0] = tubes_down[br][td];		  
			tubes_down[br][td]->connect_bief->frac_neigh_tubes[0] = 1. - ((Qdown - Qup) / Qdown_abs);
			printf("tube d %d tube up neigh %d frac %lf\n", td, tup, tubes_down[br][td]->connect_bief->frac_neigh_tubes[0]);
			tup++;
			tubes_down[br][td]->connect_bief->neigh_tubes[1] = tubes_up[br][tup];		  
			tubes_down[br][td]->connect_bief->frac_neigh_tubes[1] = (Qdown - Qup) / Qdown_abs;
			printf("tube d %d tube up neigh %d frac %lf\n", td, tup, tubes_down[br][td]->connect_bief->frac_neigh_tubes[1]);
			/****/
			td++;
			Qdown = Qdown + Qdown_abs;
			Qup = Qup + Qup_abs;
			/****/
		      }
		  }
		
		/****/
		td = 0; tup = 0; inc = 1;
		Qdown_abs = tubes_down[br][td]->param_amont->A*tubes_down[br][td]->param_amont->U; Qup_abs = tubes_up[br][tup]->param_aval->A*tubes_up[br][tup]->param_aval->U;
		Qdown = Qdown_abs; Qup = Qup_abs;
		/****/
		printf("\nup %lf dwn %lf\n", Qup_abs, Qdown_abs);
		while (tup < n_tubeup && td < n_tube) /// 2. Treatment of upward tubes (when they are less numerous than dw tubes)
		  {
		    printf("%lf %lf\n",Qup,Qdown);
		    if (Qup >= Qdown || fabs(Qdown - Qup) < EPS2_TUB)
		      {
			if (fabs(Qdown - Qup) < EPS2_TUB)
			  {
			    tubes_up[br][tup]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
			    tubes_up[br][tup]->connect_bief->type = DOWNSTREAM;
			    tubes_up[br][tup]->connect_bief->n_neigh_tubes = inc;
			    tubes_up[br][tup]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*)*inc);
			    tubes_up[br][tup]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double)*inc);
			    Qdown = Qdown - inc*Qdown_abs;
			    Qup = Qup - Qup_abs;
			    Qprev = Qup;
			    for (i=0; i<inc-1; i++)
			      {
				Qdown = Qdown + Qdown_abs;
				tubes_up[br][tup]->connect_bief->neigh_tubes[i] = tubes_down[br][td-(inc-(i+1))];
				tubes_up[br][tup]->connect_bief->frac_neigh_tubes[i] = (Qdown - Qprev) / Qup_abs;
				printf("tube up %d tube d neigh %d frac %lf\n", tup, td-(inc-(i+1)), tubes_up[br][tup]->connect_bief->frac_neigh_tubes[i]);
				Qprev = Qdown;
			      }
			    Qup = Qup + Qup_abs;
			    tubes_up[br][tup]->connect_bief->neigh_tubes[i] = tubes_down[br][td-(inc-(i+1))]; // definition of the last tube of a series
			    tubes_up[br][tup]->connect_bief->frac_neigh_tubes[i] = (Qup - Qdown) / Qup_abs;
			    printf("tube up %d tube d neigh %d frac %lf\n", tup, td-(inc-(i+1)), tubes_up[br][tup]->connect_bief->frac_neigh_tubes[i]);
			    /****/
			    tup++;
			    Qdown = Qdown + Qdown_abs;
			    Qup = Qup + Qup_abs;
			    inc = 0;
			    /****/ 
			  }
			else
			  {
			    Qdown = Qdown + Qdown_abs;
			    td++;
			    inc++;
			  }
		      }
		    else
		      {
			tubes_up[br][tup]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
			tubes_up[br][tup]->connect_bief->type = UPSTREAM;
			tubes_up[br][tup]->connect_bief->n_neigh_tubes = inc;
			tubes_up[br][tup]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*)*inc);
			tubes_up[br][tup]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double)*inc);
			Qdown = Qdown - inc*Qdown_abs;
			Qup = Qup - Qup_abs;
			Qprev = Qup;
			for (i=0; i<inc-1; i++)
			  {
			    Qdown = Qdown + Qdown_abs;
			    tubes_up[br][tup]->connect_bief->neigh_tubes[i] = tubes_down[br][td-(inc-(i+1))];
			    tubes_up[br][tup]->connect_bief->frac_neigh_tubes[i] = (Qdown - Qprev) / Qup_abs;
			    printf("tube up %d tube d neigh %d frac %lf\n", tup, td-(inc-(i+1)), tubes_up[br][tup]->connect_bief->frac_neigh_tubes[i]);
			    Qprev = Qdown;
			  }
			Qup = Qup + Qup_abs;
			tubes_up[br][tup]->connect_bief->neigh_tubes[i] = tubes_down[br][td-(inc-(i+1))]; // definition of the last tube of a series
			tubes_up[br][tup]->connect_bief->frac_neigh_tubes[i] = (Qup - Qdown) / Qup_abs;
			printf("tube up %d tube d neigh %d frac %lf\n", tup, td-(inc-(i+1)), tubes_up[br][tup]->connect_bief->frac_neigh_tubes[i]);
			/****/
			tup++;
			Qdown = Qdown + Qdown_abs;
			Qup = Qup + Qup_abs;
			inc = 1;
			/****/
		      }
		  }
	      }
	  }
	break;
	
      case DISCHARGE:
	break;
      case WATER_LEVEL:
	break;
	
	/*********************************************************************/
	/*********************  MULTI REACH CASES  ***************************/
	/*********************************************************************/
      case CONFLUENCE:
      case DIFFLUENCE:
      case CONF_DIFF:

	/* Initialisation of the tube number on the both sides of the singularity */
	n_tubeup = 0;
	for (br=0; br<sing->nupstr_reaches; br++)
	  n_tubeup = n_tubeup + chyd->p_reach[r_reach_up[br]]->n_tube;
	n_tube = 0;
	for (br=0; br<sing->ndownstr_reaches; br++)
	  n_tube = n_tube + chyd->p_reach[r_reach_down[br]]->n_tube;

	/***********************************************************************/
	/*** I. Same number of tubes from one side of the sing. to the other ***/
	/***********************************************************************/
	if (n_tubeup == n_tube) 
	  {
	    tup = 0; td = 0;
	    br = 0; brup = 0;
	    for (t=0; t<n_tube; t++)
	      if (tubes_up[brup][tup]->voisin[DOWNWARD_TUB] == NULL && tubes_down[br][td]->voisin[UPWARD_TUB] == NULL)
		{
		  if (tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL && tubes_down[br][td]->voisin[RIGHT_TUB] != NULL)
		    {
		      tubes_up[brup][tup]->voisin[DOWNWARD_TUB] = tubes_down[br][td];
		      tubes_down[br][td]->voisin[UPWARD_TUB] = tubes_up[brup][tup];
		      brup++;
		      tup = 0;
		      td++;
		    }
		  else if (tubes_down[br][td]->voisin[RIGHT_TUB] == NULL && tubes_up[brup][tup]->voisin[RIGHT_TUB] != NULL)
		    {
		      tubes_up[brup][tup]->voisin[DOWNWARD_TUB] = tubes_down[br][td];
		      tubes_down[br][td]->voisin[UPWARD_TUB] = tubes_up[brup][tup];
		      br++;
		      td = 0;
		      tup++;
		    }
		  else if (tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL && tubes_down[br][td]->voisin[RIGHT_TUB] == NULL)
		    {
		      tubes_up[brup][tup]->voisin[DOWNWARD_TUB] = tubes_down[br][td];
		      tubes_down[br][td]->voisin[UPWARD_TUB] = tubes_up[brup][tup];
		      brup++;
		      tup = 0;
		      br++;
		      td = 0;
		    }
		  else
		    {
		      tubes_up[brup][tup]->voisin[DOWNWARD_TUB] = tubes_down[br][td];
		      tubes_down[br][td]->voisin[UPWARD_TUB] = tubes_up[brup][tup];
		      tup++;
		      td++;
		    }
		}
	      else
		{
		  LP_error(stderr,"Impossible: ending tubes of reach cannot already be allocated. Quitting program.\n");
		  exit(0);
		}
	  }

	/*****************************************************************************/
	/*** II. Different number of tubes from one side of the sing. to the other ***/
	/*****************************************************************************/
	else
	  {

	    
	    /*********************************************/
	    /****/ /* FIRST: For the upward tubes */ /****/
	    td = 0; tup = 0; t1 = 0; t2 = 0; br = 0; brup = 0; inc = 1; indicator = 0;
	    Qdown_abs = tubes_down[br][td]->param_amont->A * tubes_down[br][td]->param_amont->U; Qup_abs = tubes_up[brup][tup]->param_aval->A * tubes_up[brup][tup]->param_aval->U;
	    Qdown = Qdown_abs; Qup = Qup_abs;
	    /****/
	    printf("\nQu %lf Qd %lf\n", Qup_abs, Qdown_abs);	
	    while (t1 < n_tubeup || t2 < n_tube)
	      {
		printf("%lf %lf\n",Qup,Qdown);
		/*|*******************************************************|*/
		/*v 1. Flow rate of tube DOWN > UP (over a certain reach) v*/
		/*v*******************************************************v*/
		if (Qdown_abs >= Qup_abs)
		  if (Qdown + EPS2_TUB >= Qup) /* i. cursor level */
		    {
		      tubes_up[brup][tup]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
		      tubes_up[brup][tup]->connect_bief->type = DOWNSTREAM;
		      tubes_up[brup][tup]->connect_bief->n_neigh_tubes = 1;
		      tubes_up[brup][tup]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*));
		      tubes_up[brup][tup]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double));			
		      tubes_up[brup][tup]->connect_bief->neigh_tubes[0] = tubes_down[br][td];		  
		      tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[0] = 1.;
		      printf("tube up %d %d tube d neigh %d %d frac %lf\n", brup, tup, br, td, tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[0]);
		      if (fabs(Qdown - Qup) < EPS2_TUB)
			{
			  t2++;
			  if (t2 >= n_tube)
			    break;
			  if (tubes_down[br][td]->voisin[RIGHT_TUB] == NULL)
			    {
			      br++;
			      td = 0;
			      Qdown_abs = tubes_down[br][td]->param_amont->A * tubes_down[br][td]->param_amont->U;
			    }
			  else
			    {
			      td++;
			    }
			  Qdown = Qdown + Qdown_abs;
			}
		      /****/
		      t1++;
		      if (t1 >= n_tubeup)
			break; 
		      if (tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL)
			{
			  brup++;
			  tup = 0;
			  Qup_abs = tubes_up[brup][tup]->param_aval->A * tubes_up[brup][tup]->param_aval->U;
			}
		      else
			{
			  tup++;
			}
		      Qup = Qup + Qup_abs;
		      /****/
		    }
		  else /* ii. cursor level */
		    {
		      tubes_up[brup][tup]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
		      tubes_up[brup][tup]->connect_bief->type = DOWNSTREAM;
		      if (tubes_down[br][td]->voisin[RIGHT_TUB] == NULL && t2+1 > n_tube)
			nnt = 1;
		      else
			nnt = 2;
		      tubes_up[brup][tup]->connect_bief->n_neigh_tubes = nnt;
		      tubes_up[brup][tup]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*)*nnt);
		      tubes_up[brup][tup]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double)*nnt);
		      tubes_up[brup][tup]->connect_bief->neigh_tubes[0] = tubes_down[br][td];
		      tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[0] = 1. - ((Qup - Qdown) / Qup_abs);
		      if (nnt == 1)
			tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[0] = 1.;
		      printf("tube up %d %d tube d neigh %d %d frac %lf\n", brup, tup, br, td, tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[0]);
		      if (tubes_down[br][td]->voisin[RIGHT_TUB] == NULL)
			{
			  br++;
			  td = 0;
			  t2++;
			  if (t2 >= n_tube)
			    break;
			  Qdown_abs = tubes_down[br][td]->param_amont->A * tubes_down[br][td]->param_amont->U;
			}
		      else
			{
			  td++;
			  t2++;	
			}
		      tubes_up[brup][tup]->connect_bief->neigh_tubes[1] = tubes_down[br][td];		  
		      tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[1] = (Qup - Qdown) / Qup_abs;
		      printf("tube up %d %d tube d neigh %d %d frac %lf\n", brup, tup, br, td, tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[1]);
		      /****/
		      t1++;
		      if (t1 >= n_tubeup)
			break; 
		      if (tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL)
			{
			  brup++;
			  tup = 0;
			  Qup_abs = tubes_up[brup][tup]->param_aval->A * tubes_up[brup][tup]->param_aval->U;
			}
		      else
			{
			  tup++;
			}
		      Qup = Qup + Qup_abs;
		      Qdown = Qdown + Qdown_abs;
		      /****/
		      if (fabs(Qdown - Qup) > EPS2_TUB && tubes_down[br][td]->voisin[RIGHT_TUB] == NULL && t2 < n_tube-1)
			  {
			    br++;
			    td = 0;
			    Qdown_abs = tubes_down[br][td]->param_amont->A * tubes_down[br][td]->param_amont->U;
			    br--;
			    td = chyd->p_reach[r_reach_down[br]]->n_tube - 1; 
			  }
		    }
		/***************************************************/
		/** 2. Flow rate of tube UP > DOWN (see abs val.) **/
		/***************************************************/
		else
		  if (Qup > Qdown && fabs(Qup - Qdown) > EPS2_TUB && t2+1 < n_tube) /* ii. cursor level */
		    {
		      if (tubes_down[br][td]->voisin[RIGHT_TUB] == NULL)
			{
			  br++;
			  td = 0;
			  Qdown_abs = tubes_down[br][td]->param_aval->A * tubes_down[br][td]->param_aval->U;
			}
		      else
			{
			  td++;
			}
		      inc++;
		      t2++;
		      Qdown = Qdown + Qdown_abs;
		    }
		  else /* i. cursor level */
		    {
		      tubes_up[brup][tup]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
		      tubes_up[brup][tup]->connect_bief->type = DOWNSTREAM;
		      tubes_up[brup][tup]->connect_bief->n_neigh_tubes = inc;
		      tubes_up[brup][tup]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*)*inc);
		      tubes_up[brup][tup]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double)*inc);
		      /****/
		      if (fabs(Qdown - Qup) < EPS2_TUB && indicator == 0)
			indicator = 1;
		      // Faire machine arrière !!! //
		      // Qdown = Qdown - inc*Qdown_abs;
		      for (i=inc; i>0; i--)
			{
			  Qdown = Qdown - Qdown_abs;
			  if (i == 1)
			    Qup = Qup - Qup_abs;
			  if (i == 1 || tubes_down[br][td]->voisin[LEFT_TUB] == NULL)
			    {
			      if (tubes_down[br][td]->voisin[LEFT_TUB] == NULL && fabs(Qdown - Qup) > EPS2_TUB && i != 1)
				{
				  br--;
				  td = chyd->p_reach[r_reach_down[br]]->n_tube - 1;
				  Qdown_abs = tubes_down[br][td]->param_aval->A * tubes_down[br][td]->param_aval->U;
				}
			    }
			  else
			    {
			      td--;
			    }
			}
		      Qprev = Qup;
		      ///////////////////////////////
		      for (i=0; i<inc-1; i++)
			{
			  Qdown = Qdown + Qdown_abs;
			  tubes_up[brup][tup]->connect_bief->neigh_tubes[i] = tubes_down[br][td];
			  tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[i] = (Qdown - Qprev) / Qup_abs;
			  printf("tube up %d %d tube d neigh %d %d frac %lf\n", brup, tup, br, td, tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[i]);
			  if (tubes_down[br][td]->voisin[RIGHT_TUB] == NULL)
			    {
			      br++;
			      td = 0;
			      Qdown_abs = tubes_down[br][td]->param_aval->A * tubes_down[br][td]->param_aval->U;
			    }
			  else
			    {
			      td++;
			    }
			  Qprev = Qdown;
			}
		      Qup = Qup + Qup_abs;
		      tubes_up[brup][tup]->connect_bief->neigh_tubes[i] = tubes_down[br][td]; // definition of the last tube of a series
		      if (inc == 1)
			tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[i] = 1.;
		      else
			tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[i] = (Qup - Qdown) / Qup_abs;
		      printf("tube up %d %d tube d neigh %d %d frac %lf\n", brup, tup, br, td, tubes_up[brup][tup]->connect_bief->frac_neigh_tubes[i]);
		      /****/
		      t1++;
		      if (t1 >= n_tubeup)
			break;
		      if (tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL)
			{
			  brup++;
			  tup = 0;
			  Qup_abs = tubes_up[brup][tup]->param_aval->A * tubes_up[brup][tup]->param_aval->U;
			}
		      else
			{
			  tup++;
			}
		      Qup = Qup + Qup_abs;
		      Qdown = Qdown + Qdown_abs;
		      inc = 1;
		      /****/
		      if (indicator == 1)
			{
			  indicator = 0;
			  t2++;
			  if (t2 >= n_tube)
			    break;
			  if (tubes_down[br][td]->voisin[RIGHT_TUB] == NULL)
			    {
			      br++;
			      td = 0;
			      Qdown_abs = tubes_down[br][td]->param_amont->A * tubes_down[br][td]->param_amont->U;
			    }
			  else
			    {
			      td++;
			    }
			  Qdown = Qdown + Qdown_abs;
			}
		      /****/
		    }
	      }
	    

	    /************************************************/
	    /****/ /* SECOND: For the downward tubes */ /****/
	    td = 0; tup = 0; t1 = 0; t2 = 0; br = 0; brup = 0; inc = 1; indicator = 0;
	    Qdown_abs = tubes_down[br][td]->param_amont->A * tubes_down[br][td]->param_amont->U; Qup_abs = tubes_up[brup][tup]->param_aval->A * tubes_up[brup][tup]->param_aval->U;
	    Qdown = Qdown_abs; Qup = Qup_abs;
	    /****/
	    printf("\nQu %lf Qd %lf\n", Qup_abs, Qdown_abs);	
	    while (t1 < n_tubeup || t2 < n_tube)
	      {
		printf("%lf %lf\n",Qup,Qdown);
		/*|*******************************************************|*/
		/*v 1. Flow rate of tube UP > DOWN (over a certain reach) v*/
		/*v*******************************************************v*/
		if (Qup_abs >= Qdown_abs)
		  if (Qup + EPS2_TUB >= Qdown) /* i. cursor level */
		    {
		      tubes_down[br][td]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
		      tubes_down[br][td]->connect_bief->type = UPSTREAM;
		      tubes_down[br][td]->connect_bief->n_neigh_tubes = 1;
		      tubes_down[br][td]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*));
		      tubes_down[br][td]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double));			
		      tubes_down[br][td]->connect_bief->neigh_tubes[0] = tubes_down[br][td];		  
		      tubes_down[br][td]->connect_bief->frac_neigh_tubes[0] = 1.;
		      printf("tube d %d %d tube u neigh %d %d frac %lf\n", br, td, brup, tup, tubes_down[br][td]->connect_bief->frac_neigh_tubes[0]);
		      if (fabs(Qdown - Qup) < EPS2_TUB)
			{
			  t1++; // Modified -> relocated here to have the condition below !!!!
			  if (t1 >= n_tubeup)
			    break;
			  if (tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL)
			    {
			      brup++;
			      tup = 0;
			      Qup_abs = tubes_up[brup][tup]->param_amont->A * tubes_up[brup][tup]->param_amont->U;
			      printf("Qu %lf Qd %lf\n", Qup_abs, Qdown_abs);
			    }
			  else
			    {
			      tup++;
			    }
			  Qup = Qup + Qup_abs;
			}
		      /****/
		      t2++;
		      if (t2 >= n_tube)
			break;
		      if (tubes_down[br][td]->voisin[RIGHT_TUB] == NULL)
			{
			  br++;
			  td = 0;
			  Qdown_abs = tubes_down[br][td]->param_aval->A * tubes_down[br][td]->param_aval->U;
			  printf("Qu %lf Qd %lf\n", Qup_abs, Qdown_abs);
			}
		      else
			{
			  td++;
			}
		      Qdown = Qdown + Qdown_abs;
		      /****/
		    }
		  else /* ii. cursor level */
		    {
		      tubes_down[br][td]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
		      tubes_down[br][td]->connect_bief->type = DOWNSTREAM;
		      if (tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL && t1+1 > n_tubeup)
			nnt = 1;
		      else
			nnt = 2;
		      tubes_down[br][td]->connect_bief->n_neigh_tubes = nnt;
		      tubes_down[br][td]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*)*nnt);
		      tubes_down[br][td]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double)*nnt);
		      tubes_down[br][td]->connect_bief->neigh_tubes[0] = tubes_down[br][td];		  
		      tubes_down[br][td]->connect_bief->frac_neigh_tubes[0] = 1. - ((Qdown - Qup) / Qdown_abs);
		      if (nnt == 1)
			tubes_down[br][td]->connect_bief->frac_neigh_tubes[0] = 1.;
		      printf("tube d %d %d tube u neigh %d %d frac %lf\n", br, td, brup, tup, tubes_down[br][td]->connect_bief->frac_neigh_tubes[0]);
		      if (tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL)
			{
			  brup++;
			  tup = 0;
			  t1++;
			  if (t1 >= n_tubeup)
			    break;
			  Qup_abs = tubes_up[brup][tup]->param_amont->A * tubes_up[brup][tup]->param_amont->U;
			  printf("Qu %lf Qd %lf\n", Qup_abs, Qdown_abs);
			}
		      else
			{
			  tup++;
			  t1++;	
			}
		      tubes_down[br][td]->connect_bief->neigh_tubes[1] = tubes_up[brup][tup];		  
		      tubes_down[br][td]->connect_bief->frac_neigh_tubes[1] = (Qdown - Qup) / Qdown_abs;
		      printf("tube d %d %d tube u neigh %d %d frac %lf\n", br, td, brup, tup, tubes_down[br][td]->connect_bief->frac_neigh_tubes[1]);
		      /****/
		      t2++;
		      if (t2 >= n_tube)
			break;
		      if (tubes_down[br][td]->voisin[RIGHT_TUB] == NULL)
			{
			  br++;
			  td = 0;
			  Qdown_abs = tubes_down[br][td]->param_aval->A * tubes_down[br][td]->param_aval->U;
			  printf("Qu %lf Qd %lf\n", Qup_abs, Qdown_abs);
			}
		      else
			{
			  td++;
			}
		      Qdown = Qdown + Qdown_abs;
		      Qup = Qup + Qup_abs;
		      /****/
		      if (fabs(Qdown - Qup) > EPS2_TUB && tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL && t1 < n_tubeup - 1)
			  {
			    brup++;
			    tup = 0;
			    Qup_abs = tubes_up[brup][tup]->param_amont->A * tubes_up[brup][tup]->param_amont->U;
			    brup--;
			    tup = chyd->p_reach[r_reach_up[brup]]->n_tube - 1;
			  }
		    }
		/***************************************************/
		/** 2. Flow rate of tube DOWN > UP (see abs val.) **/
		/***************************************************/
		else
		  if (Qdown > Qup && fabs(Qup - Qdown) > EPS2_TUB && t1+1 < n_tubeup) /* ii. cursor level */
		    {
		      if (tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL)
			{
			  brup++;
			  tup = 0;
			  Qup_abs = tubes_up[brup][tup]->param_aval->A * tubes_up[brup][tup]->param_aval->U;
			}
		      else
			{
			  tup++;
			}
		      inc++;
		      t1++;
		      Qup = Qup + Qup_abs;
		    }
		  else /* i. cursor level */
		    {
		      tubes_down[br][td]->connect_bief = (s_reach_connect_tub *)malloc(sizeof(s_reach_connect_tub));
		      tubes_down[br][td]->connect_bief->type = UPSTREAM;
		      tubes_down[br][td]->connect_bief->n_neigh_tubes = inc;
		      tubes_down[br][td]->connect_bief->neigh_tubes = (s_def_tub **)malloc(sizeof(s_def_tub*)*inc);
		      tubes_down[br][td]->connect_bief->frac_neigh_tubes = (double *)malloc(sizeof(double)*inc);
		      /****/
		      if (fabs(Qdown - Qup) < EPS2_TUB && indicator == 0)
			indicator = 1;
		      // Faire machine arrière !!! //
		      // Qdown = Qdown - inc*Qdown_abs;
		      for (i=inc; i>0; i--)
			{
			  Qup = Qup - Qup_abs;
			  if (i == 1)
			    Qdown = Qdown - Qdown_abs;
			  if (i == 1 || tubes_up[brup][tup]->voisin[LEFT_TUB] == NULL)
			    {
			      if (tubes_up[brup][tup]->voisin[LEFT_TUB] == NULL && fabs(Qdown - Qup) > EPS2_TUB && i != 1)
				{
				  brup--;
				  tup = chyd->p_reach[r_reach_up[brup]]->n_tube - 1;
				  Qup_abs = tubes_up[brup][tup]->param_aval->A * tubes_up[brup][tup]->param_aval->U;
				}
			    }
			  else
			    {
			      tup--;
			    }
			}
		      ///////////////////////////////
		      Qprev = Qdown;
		      for (i=0; i<inc-1; i++)
			{
			  Qup = Qup + Qup_abs;
			  tubes_down[br][td]->connect_bief->neigh_tubes[i] = tubes_up[brup][tup];
			  tubes_down[br][td]->connect_bief->frac_neigh_tubes[i] = (Qup - Qprev) / Qdown_abs;
			  printf("tube d %d %d tube u neigh %d %d frac %lf\n", br, td, brup, tup, tubes_down[br][td]->connect_bief->frac_neigh_tubes[i]);
			  if (tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL)
			    {
			      brup++;
			      tup = 0;
			      Qup_abs = tubes_up[brup][tup]->param_aval->A * tubes_up[brup][tup]->param_aval->U;
			      printf("Qu %lf Qd %lf\n", Qup_abs, Qdown_abs);
			    }
			  else
			    {
			      tup++;
			    }
			  Qprev = Qup;
			}
		      Qdown = Qdown + Qdown_abs;
		      tubes_down[br][td]->connect_bief->neigh_tubes[i] = tubes_up[brup][tup]; // definition of the last tube of a series
		      if (inc == 1)
			tubes_down[br][td]->connect_bief->frac_neigh_tubes[i] = 1.;
		      else
			tubes_down[br][td]->connect_bief->frac_neigh_tubes[i] = (Qdown - Qup) / Qdown_abs;
		      printf("tube d %d %d tube u neigh %d %d frac %lf\n", br, td, brup, tup, tubes_down[br][td]->connect_bief->frac_neigh_tubes[i]);
		      /****/
		      t2++;
		      if (t2 >= n_tube)
			break;
		      if (tubes_down[br][td]->voisin[RIGHT_TUB] == NULL)
			{
			  br++;
			  td = 0;
			  Qdown_abs = tubes_down[br][td]->param_aval->A * tubes_down[br][td]->param_aval->U;
			  printf("Qu %lf Qd %lf\n", Qup_abs, Qdown_abs);
			}
		      else
			{
			  td++;
			}
		      Qdown = Qdown + Qdown_abs;
		      Qup = Qup + Qup_abs;
		      inc = 1;
		      /****/
		      if (indicator == 1)
			{
			  indicator = 0;
			  t1++;
			  if (t1 >= n_tubeup)
			    break;
			  if (tubes_up[brup][tup]->voisin[RIGHT_TUB] == NULL)
			    {
			      brup++;
			      tup = 0;
			      Qup_abs = tubes_up[brup][tup]->param_amont->A * tubes_up[brup][tup]->param_amont->U;
			    }
			  else
			    {
			      tup++;
			    }
			  Qup = Qup + Qup_abs;
			}
		      /****/
		    }
	      }
	  }
	break;
	
      default:
	printf("CAUTION: NONE OF THE TYPES HAS BEEN FOUND !!!\n");
	break;
      }
    }
}

void update_tube_domain_TUB(s_def_tub ****tube, s_chyd *chyd) // This is a partnership with libhyd®
{
  int r, e, t;
  int n_tube;
  s_reach_hyd *reach;
  s_element_hyd *pele;
  s_face_hyd **face;

  for (r=0; r<chyd->counter->nreaches; r++)
    {
      reach = chyd->p_reach[r];
      n_tube = reach->n_tube;
      for(e=0; e<reach->nele; e++)
	{
	  pele = reach->p_ele[e];
	  face = pele->face[X_HYD];
	  for (t=0; t<n_tube; t++)
	    update_param_over_one_face_TUB(tube[r][e], face, n_tube);
	}
    }
}

void reduce_tube_number(s_chyd *chyd)
{
  int s, br;
  int n_sing;
  s_singularity_hyd *sing;
  
  n_sing = chyd->counter->nsing;
  for (s=0; s<n_sing; s++)
    {
      sing = chyd->p_sing[s];
      if (sing->type == DIFFLUENCE)
	  for (br=0; br<sing->ndownstr_reaches; br++)
	    {
	      sing->p_reach[DOWNSTREAM][br]->n_tube = sing->p_reach[DOWNSTREAM][br]->n_tube/2;
	      if(sing->p_reach[DOWNSTREAM][br]->n_tube < NTUBE_MIN)
		sing->p_reach[DOWNSTREAM][br]->n_tube = NTUBE_MIN;
	    }
    }
}
