/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libtube
* FILE NAME: tube_properties.c
* BRANCH NAME: main
* 
* CONTRIBUTORS: Aur�lien BORDET, Shuaitao WANG, Nicolas FLIPO
* 
* PROJECT MANAGER: Nicolas FLIPO
*
* LIBRARY BRIEF DESCRIPTION: Pseudo-2D river mesh that transforms 1D hydraulics outputs into 2D ones based on equidischarge tubes in each cross-section. 
* Possibility to parameterize the number of tubes locally.
*
* ANSI C library developed at the Geosciences and geoengineering Department, joint research center of Mines Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2023 Contributors to the libtube library. 
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          
* 
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"

#include "HYD.h"
#include "TUB.h"

s_cross_section_tub *profile_building_TUB(s_face_hyd *face, int n_tube)
{
  int i, passage;
  double U, Q_tot, w_level, Ks, Rh43, J0, tot_width;
  double U_l, U_r, Q_l, Q_r, *param_l, *param_r, *sect_param;
  double delta, dF;
  double Yinit, Y0, Y1, F0, F1, B0, B1;
  double height_diff = 0.;

  s_pointAbscZ_hyd *p;
  s_pointXYZ_hyd *q;
  
  s_cross_section_tub *profile;
  double **tube_coord;
  double mid_flow;

  /* allocation des coordonnées pour chaque tube d'un profil */
  tube_coord = (double **)malloc(sizeof(double*)*n_tube);
  for (i=0; i<n_tube; i++)
    tube_coord[i] = (double *)malloc(sizeof(double)*2);

  //U = face->hydro->Vel;
  Q_tot = face->hydro->Q[T_HYD];
  w_level = face->hydro->Z[T_HYD];
  Ks = face->hydro->ks;
  //Rh43 = face->hydro->rhyd43;
  //J0 = energy_grad_line_slope_TUB(U, Ks, Rh43);
  check_geom_chains(face); /*** WARNING: this routine modifies a structure of libhyd into the code ***/

  /* affectation de la structure de profils et pré conditionnement de l'algo */
  profile = (s_cross_section_tub *)malloc(sizeof(s_cross_section_tub));
  profile->type = face->geometry->type;
  /*switch(profile->type){
  case ABSC_Z :
    profile->pptsAbscZ = face->geometry->p_pointsAbscZ[0];
    break;
  case X_Y_Z :
    profile->pptsXYZ = face->geometry->p_pointsXYZ[0];
    break;
    }*/
  //tot_width = profile_overflow(w_level, profile, height_diff); /*** WARNING: this routine modifies a structure of libhyd into the code ***/
  tot_width = profile_filling(w_level, face, profile, height_diff); // SW 26/03/2021 height_diff = 0 ? not used
  sect_param = wet_param_from_left_bank_TUB(w_level, tot_width, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
  U = Q_tot / sect_param[0];
  Rh43 = pow(sect_param[0]/sect_param[1], 4./3.);
  
  /* SW 31/03/2021 need to free sect_param which is allocated in wet_param_from_left_bank_TUB*/
  free(sect_param);
  sect_param = NULL;
  
  J0 = energy_grad_line_slope_TUB(U, Ks, Rh43);
  mid_flow = equi_flow_rate_distance_TUB(profile, height_diff, tot_width, U, w_level, Ks, Rh43);
  
  Y0 = 0.; // !!!-> Initialization from left bank...
  for (i=0; i<n_tube-1; i++)
    {
      /*** Algorithme de Broyden ***/
      /* Init. */
      dF = 1.;
      Yinit = Y0;
      /***********/
      if (i+1 <= n_tube/2)
	{
	  B0 = -1.;
	  param_l = wet_param_from_left_bank_TUB(w_level, Y0, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
	  U_l = left_integrated_velocity_TUB(param_l, Ks, J0);      
	  Q_l = cumulated_flow_rate_from_left_bank_TUB(U_l, param_l[0]);
	  
	  /* SW 31/03/2021 need to free param_l which is allocated in wet_param_from_left_bank_TUB*/
	  free(param_l);
	  param_l = NULL;
	  
	  /***********/
	  F0 = (Q_tot/(double)n_tube)*(i+1) - Q_l;
	}
      else
	{
	  B0 = 1.;
	  if (n_tube%2 == 1 && i == n_tube/2)
	    Y0 = mid_flow;
	  param_r = wet_param_from_right_bank_TUB(w_level, Y0, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
	  U_r = right_integrated_velocity_TUB(param_r, Ks, J0);
	  Q_r = cumulated_flow_rate_from_right_bank_TUB(U_r, param_r[0]);
	  
	  /* SW 31/03/2021 need to free param_l which is allocated in wet_param_from_right_bank_TUB*/
	  free(param_r);
	  param_r = NULL;
	  
	  /***********/
	  F0 = (Q_tot/(double)n_tube)*(i+1) - (Q_tot - Q_r);
	}
      /* Boucle */
      passage = 0;
      while (fabs(F0) > EPS1_TUB)
	{
	  /*** Convergence test for the Broyden algo. ***/
	  passage++;
	  if (passage > DIV_PARAM) // New algorithm -> .../2
	    {
	      //printf("NO CONVERGENCE OF BROYDEN ALGORITHM -> SWITCHING TO SIMPLER ALGO...\n");/////////////////////////////////////////////
	      Y0 = back_up_algorithm(i, n_tube, Q_tot, tot_width, Yinit, mid_flow, w_level, profile, Ks, J0, height_diff);
	      break;
	    }
	  /*** If pair number of tubes, grab the coordinates of mid flow rate point when ith tube reaches n_tube/2 ***/
	  if (n_tube%2 == 0 && i+1 == n_tube/2)
	    {
	      Y0 = mid_flow;
	      break;
	    }
	  delta = -(F0/B0);
	  Y1 = Y0 + delta;
	  /***********/
	  if (i+1 <= n_tube/2)
	    {
	      param_l = wet_param_from_left_bank_TUB(w_level, Y1, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
	      U_l = left_integrated_velocity_TUB(param_l, Ks, J0);      
	      Q_l = cumulated_flow_rate_from_left_bank_TUB(U_l, param_l[0]);
	      
	      /* SW 31/03/2021 need to free param_l which is allocated in wet_param_from_left_bank_TUB*/
	      free(param_l);
	      param_l = NULL;
	      	      
	      /***********/
	      F1 = (Q_tot/(double)n_tube)*(i+1) - Q_l;
	    }
	  else
	    {
	      param_r = wet_param_from_right_bank_TUB(w_level, Y1, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
	      U_r = right_integrated_velocity_TUB(param_r, Ks, J0);
	      Q_r = cumulated_flow_rate_from_right_bank_TUB(U_r, param_r[0]);
	      
	      /* SW 31/03/2021 need to free param_r which is allocated in wet_param_from_right_bank_TUB*/
	      free(param_r);
	      param_r = NULL;
	      
	      /***********/
	      F1 = (Q_tot/(double)n_tube)*(i+1) - (Q_tot - Q_r);
	    }
	  dF = F1 - F0;
	  B1 = B0 + (dF - B0*delta)/delta;
	  /* réaffectation des paramètres de l'algo */
	  Y0 = Y1;
	  F0 = F1;
	  B0 = B1;
	  //printf("delta: %lf, dF: %lf, rslt algo: %lf\n",delta,dF,Y0);
	}
      tube_coord[i][0] = Yinit;
      tube_coord[i][1] = Y0;
      if (tube_coord[i][1] - tube_coord[i][0] < 1.)
	{
	  printf("WARNING:\n");
	  printf("tube %d Y0 %lf -- Y1 %lf\n",i,tube_coord[i][0],tube_coord[i][1]);
	  switch(profile->type){
	  case ABSC_Z :
	    p = profile->pptsAbscZ;
	    while (p->prev != NULL)
	      p = p->prev;
	    while (p->next != NULL)
	      {
		printf("absc %lf z %lf\n",p->absc,p->z);
		p = p->next;
	      }
	    break;
	  case X_Y_Z :
	    q = profile->pptsXYZ;
	    while (q->prev != NULL)
	      q = q->prev;
	    while (q->next != NULL)
	      {
		printf("x %lf y %lf z %lf\n",q->x,q->y,q->z);
		q = q->next;
	      }
	    break;
	  }
	} 
    }
  tube_coord[i][0] = Y0;
  tube_coord[i][1] = tot_width;
  if (tube_coord[i][1] - tube_coord[i][0] < 1.)
    {
      printf("WARNING:\n");
      printf("tube %d Y0 %lf -- Y1 %lf\n",i,tube_coord[i][0],tube_coord[i][1]);
      switch(profile->type){
      case ABSC_Z :
	p = profile->pptsAbscZ;
	while (p->prev != NULL)
	  p = p->prev;
	while (p->next != NULL)
	  {
	    printf("absc %lf z %lf\n",p->absc,p->z);
	    p = p->next;
	  }
	break;
      case X_Y_Z :
	q = profile->pptsXYZ;
	while (q->prev != NULL)
	  q = q->prev;
	while (q->next != NULL)
	  {
	    printf("x %lf y %lf z %lf\n",q->x,q->y,q->z);
	    q = q->next;
	  }
	break;
      }
    }
  
  profile->n_tube = n_tube;
  profile->p_coord_tube = tube_coord;
  profile->Q = Q_tot;
  profile->w_level = w_level;
  profile->J0 = J0;
  //printf("w_level %lf q %lf\n",w_level,Q_tot);
  return profile;
}

double back_up_algorithm(double i, double n_tube, double Q_tot, double tot_width, double Yinit, double mid_flow, double w_level, s_cross_section_tub *profile, double Ks, double J0, double height_diff)
{
  double Y0, Y1, F1, mid;
  double *param_l, *param_r;
  double U_l, Q_l, U_r, Q_r;

  if (mid_flow != mid_flow)
    {
      printf("OUPS... -> middle flow is NaN\n");
    }
  
  /* Init. */
  Y0 = Yinit;
  F1 = 1.;
  if (i+1 <= n_tube/2)
    Y1 = mid_flow;
  else
    Y1 = tot_width;
  /* Algo. */
  while (fabs(F1) > EPS1_TUB)
    {
      //printf("Y0 %lf Y1 %lf F1 %lf\n", Y0, Y1, F1);
      mid = (Y0 + Y1)/2.;
      if (i+1 <= n_tube/2)
	{
	  param_l = wet_param_from_left_bank_TUB(w_level, mid, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
	  U_l = left_integrated_velocity_TUB(param_l, Ks, J0);      
	  Q_l = cumulated_flow_rate_from_left_bank_TUB(U_l, param_l[0]);
	  
	  /* SW 31/03/2021 need to free param_l which is allocated in wet_param_from_left_bank_TUB*/
	  free(param_l);
	  param_l = NULL;	  
	  
	  /***********/
	  F1 = (Q_tot/(double)n_tube)*(i+1) - Q_l;
	  if (F1 > 0.)
	    Y0 = mid;
	  else
	    Y1 = mid;
	}
      else
	{
	  param_r = wet_param_from_right_bank_TUB(w_level, mid, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
	  U_r = right_integrated_velocity_TUB(param_r, Ks, J0);
	  Q_r = cumulated_flow_rate_from_right_bank_TUB(U_r, param_r[0]);
	  
	  /* SW 31/03/2021 need to free param_r which is allocated in wet_param_from_right_bank_TUB*/
	  free(param_r);
	  param_r = NULL;
	  
	  /***********/
	  F1 = (Q_tot/(double)n_tube)*(i+1) - (Q_tot - Q_r);
	  if (F1 < 0.)
	    Y1 = mid;
	  else
	    Y0 = mid;
	}    
    }
  /* Results */
  return Y0;
}

s_cross_section_tub *backup_profile_building_TUB(s_face_hyd **biface, int n_tube) // in case last downward face of reach dx = 0
{
  //printf("BACKUP PROFILE BUILD ENTERED !\n");/////////////////////////////////////////////////////////////////////////////////  
  int i, passage;
  double U, Q_tot, w_level, Ks, Rh43, J0, tot_width;
  double U_l, U_r, Q_l, Q_r, *param_l, *param_r, *sect_param;
  double delta, dF;
  double Yinit, Y0, Y1, F0, F1, B0, B1;
  double height_diff = 0.;

  s_pointAbscZ_hyd *p;
  s_pointXYZ_hyd *q;
  
  s_cross_section_tub *profile;
  double **tube_coord;
  double mid_flow;
  s_face_hyd *face, *tface; // tface = treated face, that corresponds to the last downward face 

  /* allocation des coordonnées pour chaque tube d'un profil */
  tube_coord = (double **)malloc(sizeof(double*)*n_tube);
  for (i=0; i<n_tube; i++)
    tube_coord[i] = (double *)malloc(sizeof(double)*2);
  profile = (s_cross_section_tub *)malloc(sizeof(s_cross_section_tub));

  face = biface[0];
  tface = biface[1];
  check_geom_chains(face); /*** WARNING: this routine modifies a structure of libhyd into the code ***/
  //check_geom_chains(tface); --> useless !!!
  //U = tface->hydro->Vel;
  Q_tot = tface->hydro->Q[T_HYD];
  w_level = tface->hydro->Z[T_HYD];
  Ks = face->hydro->ks;
  //Rh43 = tface->hydro->rhyd43;
  //J0 = energy_grad_line_slope_TUB(U, Ks, Rh43);
  height_diff = face->description->Zbottom - tface->description->Zbottom;
  
  /* affectation de la structure de profils et pré conditionnement de l'algo */
  profile = (s_cross_section_tub *)malloc(sizeof(s_cross_section_tub));
  profile->type = face->geometry->type;
  /*switch(profile->type){
  case ABSC_Z :
    profile->pptsAbscZ = face->geometry->p_pointsAbscZ[0];
    break;
  case X_Y_Z :
    profile->pptsXYZ = face->geometry->p_pointsXYZ[0];
    break;
    }*/
  //tot_width = profile_overflow(w_level, profile, height_diff); /*** WARNING: this routine modifies a structure of libhyd into the code ***/
  tot_width = profile_filling(w_level, face, profile, height_diff);
  sect_param = wet_param_from_left_bank_TUB(w_level, tot_width, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
  U = Q_tot / sect_param[0];
  Rh43 = pow(sect_param[0]/sect_param[1], 4./3.);
  
  /* SW 31/03/2021 need to free sect_param which is allocated in wet_param_from_left_bank_TUB*/
  free(sect_param);
  sect_param = NULL;
  
  J0 = energy_grad_line_slope_TUB(U, Ks, Rh43);
  mid_flow = equi_flow_rate_distance_TUB(profile, height_diff, tot_width, U, w_level, Ks, Rh43);

  Y0 = 0.; // !!!-> Initialization from left bank...
  for (i=0; i<n_tube-1; i++)
    {
      /*** Algorithme de Broyden ***/
      /* Init. */
      dF = 1.;
      Yinit = Y0;
      /***********/
      if (i+1 <= n_tube/2)
	{
	  B0 = -1.;
	  param_l = wet_param_from_left_bank_TUB(w_level, Y0, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
	  U_l = left_integrated_velocity_TUB(param_l, Ks, J0);      
	  Q_l = cumulated_flow_rate_from_left_bank_TUB(U_l, param_l[0]);
	  
	  /* SW 31/03/2021 need to free param_l which is allocated in wet_param_from_left_bank_TUB*/
	  free(param_l);
	  param_l = NULL;
	  
	  /***********/
	  F0 = (Q_tot/((double)n_tube))*(i+1) - Q_l;
	}
      else
	{
	  B0 = 1.;
	  if (n_tube%2 == 1 && i == n_tube/2)
	    Y0 = mid_flow;
	  param_r = wet_param_from_right_bank_TUB(w_level, Y0, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
	  U_r = right_integrated_velocity_TUB(param_r, Ks, J0);
	  Q_r = cumulated_flow_rate_from_right_bank_TUB(U_r, param_r[0]);
	  
	  /* SW 31/03/2021 need to free param_r which is allocated in wet_param_from_right_bank_TUB*/
	  free(param_r);
	  param_r = NULL;
	  
	  /***********/
	  F0 = (Q_tot/(double)n_tube)*(i+1) - (Q_tot - Q_r);
	}
      /* Boucle */
      passage = 0;
      while (fabs(F0) > EPS1_TUB)
	{
	  /*** Convergence test for the Broyden algo. ***/
	  passage++;
	  if (passage > DIV_PARAM) // New algorithm -> .../2
	    {
	      //printf("NO CONVERGENCE OF SECOND BROYDEN ALGORITHM -> SWITCHING TO SIMPLER ALGO...\n");/////////////////////////////////////////////
	      Y0 = back_up_algorithm(i, n_tube, Q_tot, tot_width, Yinit, mid_flow, w_level, profile, Ks, J0, height_diff);
	      break;
	    }
	  /*** If pair number of tubes, grab the coordinates of mid flow rate point when ith tube reaches n_tube/2 ***/
	  if (n_tube%2 == 0 && i+1 == n_tube/2)
	    {
	      Y0 = mid_flow;
	      break;
	    }
	  delta = -(F0/B0);
	  Y1 = Y0 + delta;
	  /***********/
	  if (i+1 <= n_tube/2)
	    {
	      param_l = wet_param_from_left_bank_TUB(w_level, Y1, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
	      U_l = left_integrated_velocity_TUB(param_l, Ks, J0);      
	      Q_l = cumulated_flow_rate_from_left_bank_TUB(U_l, param_l[0]);
	      
	      /* SW 31/03/2021 need to free param_l which is allocated in wet_param_from_left_bank_TUB*/
	      free(param_l);
	      param_l = NULL;
	      
	      
	      /***********/
	      F1 = (Q_tot/(double)n_tube)*(i+1) - Q_l;
	    }
	  else
	    {
	      param_r = wet_param_from_right_bank_TUB(w_level, Y1, profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
	      U_r = right_integrated_velocity_TUB(param_r, Ks, J0);
	      Q_r = cumulated_flow_rate_from_right_bank_TUB(U_r, param_r[0]);
	      
	      /* SW 31/03/2021 need to free param_r which is allocated in wet_param_from_right_bank_TUB*/
	      free(param_r);
	      param_r = NULL;
	      
	      /***********/
	      F1 = (Q_tot/(double)n_tube)*(i+1) - (Q_tot - Q_r);
	    }
	  dF = F1 - F0;
	  B1 = B0 + (dF - B0*delta)/delta;
	  /* réaffectation des paramètres de l'algo */
	  Y0 = Y1;
	  F0 = F1;
	  B0 = B1;
	  //printf("delta: %lf, dF: %lf, rslt algo: %lf\n",delta,dF,Y0);
	}
      tube_coord[i][0] = Yinit;
      tube_coord[i][1] = Y0;
      if (tube_coord[i][1] - tube_coord[i][0] < 1.)
	{
	  printf("WARNING:\n");
	  printf("tube %d Y0 %lf -- Y1 %lf\n",i,tube_coord[i][0],tube_coord[i][1]);
	  switch(profile->type){
	  case ABSC_Z :
	    p = profile->pptsAbscZ;
	    while (p->prev != NULL)
	      p = p->prev;
	    while (p->next != NULL)
	      {
		printf("absc %lf z %lf\n",p->absc,p->z);
		p = p->next;
	      }
	    printf("absc %lf z %lf\n",p->absc,p->z);
	    break;
	  case X_Y_Z :
	    q = profile->pptsXYZ;
	    while (q->prev != NULL)
	      q = q->prev;
	    while (q->next != NULL)
	      {
		printf("x %lf y %lf z %lf\n",q->x,q->y,q->z);
		q = q->next;
	      }
	    printf("x %lf y %lf z %lf\n",q->x,q->y,q->z);
	    break;
	  }
	}
    }
  tube_coord[i][0] = Y0;
  tube_coord[i][1] = tot_width;
  if (tube_coord[i][1] - tube_coord[i][0] < 1.)
    {
      printf("WARNING:\n");
      printf("tube %d Y0 %lf -- Y1 %lf\n",i,tube_coord[i][0],tube_coord[i][1]);
      switch(profile->type){
      case ABSC_Z :
	p = profile->pptsAbscZ;
	while (p->prev != NULL)
	  p = p->prev;
	while (p->next != NULL)
	  {
	    printf("absc %lf z %lf\n",p->absc,p->z);
	    p = p->next;
	  }
	break;
      case X_Y_Z :
	q = profile->pptsXYZ;
	while (q->prev != NULL)
	  q = q->prev;
	while (q->next != NULL)
	  {
	    printf("x %lf y %lf z %lf\n",q->x,q->y,q->z);
	    q = q->next;
	  }
	break;
      }
    }
  
  profile->n_tube = n_tube;
  profile->p_coord_tube = tube_coord;
  profile->Q = Q_tot;
  profile->w_level = w_level;
  profile->J0 = J0;
  //printf("w_level %lf q %lf\n",w_level,Q_tot);
  return profile;  
}

s_tube_param_tub **tubes_param_over_one_face_TUB(s_cross_section_tub *profile)
{
  int i, n_tube;
  double Qmean, w_level, J;
  double A0, A1;
  s_tube_param_tub **tube_param;
  double *param;
  double height_diff;

  n_tube = profile->n_tube;
  Qmean = profile->Q;
  w_level = profile->w_level;
  J = profile->J0; 
  A0 = 0.;
  height_diff = 0.;
  
  tube_param = (s_tube_param_tub **)malloc(sizeof(s_tube_param_tub*)*n_tube);
  for (i=0; i<n_tube; i++)
    {
      tube_param[i] = (s_tube_param_tub *)malloc(sizeof(s_tube_param_tub)); 
      tube_param[i]->pcoord = profile->p_coord_tube[i];
      param = wet_param_from_left_bank_TUB(w_level, tube_param[i]->pcoord[1], profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
      A1 = param[0];
      
      /* SW 31/03/2021 need to free param which is allocated in wet_param_from_left_bank_TUB*/
      free(param);
      param = NULL;
   
      tube_param[i]->A = A1 - A0;
      A0 = A1;
      tube_param[i]->U = (Qmean/(double)n_tube)/tube_param[i]->A;
      tube_param[i]->dy = tube_param[i]->pcoord[1] - tube_param[i]->pcoord[0];
      tube_param[i]->H = tube_param[i]->A/(tube_param[i]->dy);
      tube_param[i]->U_star = pow(GRAVITY*tube_param[i]->H*J,1./2.);
    }

  return tube_param;
}

void update_param_over_one_face_TUB(s_def_tub **tube, s_face_hyd **face, int n_tube) // tube's geometry does not change
{
  int i;
  double Qmean, w_level, J, Ks, R43;
  double A0, A1, P0, P1;
  double *param;
  double height_diff = 0;
  s_cross_section_tub *profile;

  profile = (s_cross_section_tub *)malloc(sizeof(s_cross_section_tub));

  /* Amont */
  profile->type = face[0]->geometry->type;
  switch(profile->type){
  case ABSC_Z :
    profile->pptsAbscZ = face[0]->geometry->p_pointsAbscZ[0];
    break;
  case X_Y_Z :
    profile->pptsXYZ = face[0]->geometry->p_pointsXYZ[0];
    break;
  }
  Qmean = face[0]->hydro->Q[T_HYD];
  w_level = face[0]->hydro->Z[T_HYD];
  Ks = face[0]->hydro->ks;
  P0 = 0.;
  A0 = 0.; 
  for (i=0; i<n_tube; i++)
    { 
      param = wet_param_from_left_bank_TUB(w_level, tube[i]->param_amont->pcoord[1], profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
      A1 = param[0];
      P1 = param[1] - P0;
      tube[i]->param_amont->A = A1 - A0;
      A0 = A1;
      P0 = param[1];
      R43 = pow(tube[i]->param_amont->A/P1, 4./3.);
      tube[i]->param_amont->U = (Qmean/(double)n_tube)/tube[i]->param_amont->A;
      tube[i]->param_amont->H = tube[i]->param_amont->A/(tube[i]->param_amont->dy);
      J = tube[i]->param_amont->U*tube[i]->param_amont->U / (Ks*Ks * R43);
      tube[i]->param_amont->U_star = pow(GRAVITY*tube[i]->param_amont->H*J,1./2.);
      /* SW 31/30/2021 need to free param which is allocated in wet_param_from_left_bank_TUB. If not, memory leak */
      free(param);
      param = NULL;
    }

  /* Aval */
  switch(face[1]->geometry->type){
  case ABSC_Z :
    if (face[1]->geometry->p_pointsAbscZ == NULL)
      height_diff = face[0]->description->Zbottom - face[1]->description->Zbottom;
    else
      {
	profile->pptsAbscZ = face[1]->geometry->p_pointsAbscZ[0];
	profile->type = face[1]->geometry->type;
      }
    break;
  case X_Y_Z :
    if (face[1]->geometry->p_pointsXYZ == NULL)
      height_diff = face[0]->description->Zbottom - face[1]->description->Zbottom;
    else
      {
	profile->pptsXYZ = face[1]->geometry->p_pointsXYZ[0];
	profile->type = face[1]->geometry->type;
      }
    break;
  }
  Qmean = face[1]->hydro->Q[T_HYD];
  w_level = face[1]->hydro->Z[T_HYD];
  Ks = face[1]->hydro->ks;
  P0 = 0.;
  A0 = 0.; 
  for (i=0; i<n_tube; i++)
    { 
      param = wet_param_from_left_bank_TUB(w_level, tube[i]->param_aval->pcoord[1], profile, height_diff); // SW 31/03/2021 ATTENTION! memory leak
      A1 = param[0];
      P1 = param[1] - P0;
      tube[i]->param_aval->A = A1 - A0;
      A0 = A1;
      P0 = param[1];
      R43 = pow(tube[i]->param_aval->A/P1, 4./3.);
      tube[i]->param_aval->U = (Qmean/(double)n_tube)/tube[i]->param_aval->A;
      tube[i]->param_aval->H = tube[i]->param_aval->A/(tube[i]->param_aval->dy);
      J = tube[i]->param_aval->U*tube[i]->param_aval->U / (Ks*Ks * R43);
      tube[i]->param_aval->U_star = pow(GRAVITY*tube[i]->param_aval->H*J,1./2.);
      /* SW 31/30/2021 need to free param which is allocated in wet_param_from_left_bank_TUB */
      free(param);
      param = NULL;
    }
  
  free(profile);
}

void check_geom_chains(s_face_hyd *face)
{
  int type, p, npts;
  s_geometry_hyd *geometry;
  
  type = face->geometry->type;
  npts = face->geometry->npts;
  geometry = face->geometry;
  

  switch(type){
  case ABSC_Z :
    if (geometry->p_pointsAbscZ[0]->next == NULL)
      for (p=npts-1; p>=0; p--)
	{
	  //if (geometry->p_pointsAbscZ[npts-1]->absc > 0)
	  //  geometry->p_pointsAbscZ[p]->absc = -geometry->p_pointsAbscZ[p]->absc;
	  if (p == npts-1)
	    geometry->p_pointsAbscZ[p]->next = geometry->p_pointsAbscZ[p-1];
	  else if (p == 0)
	    geometry->p_pointsAbscZ[p]->prev = geometry->p_pointsAbscZ[p+1];
	  else
	    {
	      geometry->p_pointsAbscZ[p]->prev = geometry->p_pointsAbscZ[p+1];
	      geometry->p_pointsAbscZ[p]->next = geometry->p_pointsAbscZ[p-1];
	    }
	  printf("check: absc %lf z %lf\n",geometry->p_pointsAbscZ[p]->absc,geometry->p_pointsAbscZ[p]->z);
	}
    break;
  case X_Y_Z :
    //printf("case xyz\n");
    /*if (geometry->p_pointsXYZ[0]->next == NULL)
      for (p=0; p<npts; p++)
      if (p==0)
      geometry->p_pointsXYZ[p]->next = geometry->p_pointsXYZ[p+1];
      else if (p = npts-1)
      geometry->p_pointsXYZ[p]->prev = geometry->p_pointsXYZ[p-1];
      else
      {
      geometry->p_pointsXYZ[p]->prev = geometry->p_pointsXYZ[p-1];
      geometry->p_pointsXYZ[p]->next = geometry->p_pointsXYZ[p+1];
      }*/
    break;
  }
}
